/**
 * @constructor
 */
function Cache() {
    this._data = {};
}
//Y luego le agregamos los métodos:

/**
 * Agrega o reemplaza un elemento del
 * cache
 *
 * @param {String} key
 * @param {Object} value
 */
Cache.prototype.set = function(key, value) {
    this._data[key] = value;
};

/**
 * Agrega o reemplaza un elemento del
 * cache
 *
 * @param {String} key
 * @return {Object} value
 */
Cache.prototype.get = function(key) {
    return this._data[key];
};


let session = window.sessionStorage,
			log = console && console.log || function(){};


