# [ES] Plugin Wordpress Staff CIAT

Este proyecto implementa un plugin para el sitio web del Centro Internacional de Agricultura  Tropical CIAT, para presentar de su personal y 
colaboradores en su sitio weby algunas otras funcionalidades. Entre estas el plugin permite consultar  


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites/Requisitos Previos

What things you need to install the software and how to install them

```
Give examples
```

### Installing/Instalacion

A step by step series of examples that tell you have to get a development env running


Fueron creados los siguientes shotCodes para ser utilizados en las paginas wordpress
con diferentes funcionalidades:

* **General Staff** - *scPageStaff* - [PurpleBooth](https://github.com/PurpleBooth)
* **Regions Staff** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)
* **Topics Staff** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)


Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Contributing/Contribuyendo

Por favor lea la guia de contribucion. [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) para obtener mas detalles 

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/haimer.barbetti/Staff-website-Ciat2016/tags). 

## Authors
* **Haimer Barbetti** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)
* **Luis Alejandro Marulanda** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* -