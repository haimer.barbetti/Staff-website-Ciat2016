<?php
/*
Plugin Name: Staff CIAT
Plugin URI: https://gitlab.com/haimer.barbetti/Staff-website-Ciat2016.git
Description: This project implements a plugin for the web site of the Organization CIAT, for the presentation of the staff in several of its website and other functionalities.
Version: 1.1.7 
Author: CIAT - Haimer Barbetti | Alejandro Marulanda
Author URI:  https://gitlab.com/haimer.barbetti
License: GPLv2
Text Domain: StaffCIAT
*/

/*
Copyright (C) 2016 CIAT

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

This program staff CIAT wordpress plugin in particular and the information
resulting from its implementation and/or processes, such as documents,
photos and images are the property of the Organization CIAT International
Center for Tropical Agriculture and registered with exclusive copyright of
the same organization.
*/
defined( 'ABSPATH' ) or die( 'Not Access !' );

define('WP_DB_ROW_CONFIG_ID','');
define('WP_URI_TEST_','');
define('WP_URI_PRODUCTION_','');
define('WP_REGIONS_STAFF_','');
define('WP_SUB_REGIONS_STAFF_','');
define('WP_LAST_UPDATE_STAFF_','');

define('WP_CGIAR_RESEARCH_PROGRAMS_','');
define('WP_MANAGEMENT_TEAM_','');
define('WP_OFFICE_OF_THE_DIRECTOR_GENERAL_','');
define('WP_RESEARCH_STAFF_','');
define('WP_RESEARCH_SUPPORT_','');
define('WP_RESEARCH_THEME_AND_PROGRAM_LEADERS_','');

define('WP_PAISES_ALL','');
define('WP_CIUDADES_ALL','');
define('WP_REGIONES_ALL','');
define('WP__plugin_url', plugin_dir_url(__FILE__) );
define('WP__plugin_dir', plugin_dir_path(__FILE__));
define('WP_URI_TEST',"http://beta.ciat.cgiar.org/UBWInformation/jaxrs/personnelInformation/");

require_once WP__plugin_dir.'/app/core/interfaces/Plugin.php';
require_once WP__plugin_dir.'app/core/includes/constants.php';
require_once (WP__plugin_dir.'app/core/controller/StaffController.php');
require_once WP__plugin_dir.'app/core/includes/cache.php';

class StaffCIAT  implements Plugin
{
    protected $plugin_data;
    protected $plugin_name;
    protected $plugin_uri_test_env;
    protected $plugin_uri_prod_env;
    protected $plugin_nonce;
    protected $templates;
	
	public function __construct() 
    {
		global $wpdb;
        $this->templates = array();
		$this->plugin_data=get_plugin_data(__FILE__);
		$this->plugin_name=$this->plugin_data['Name'];
		$table_name_config = $wpdb->prefix .str_replace(' ','_',$this->plugin_name)."_config";
		$this->init();
        
	}
	
    public function initEnvVars()
    {
		global $wpdb;
		$table_name_config = $wpdb->prefix .str_replace(' ','_',$this->plugin_name)."_config";
        $config= $wpdb->get_row( "SELECT COUNT(*) FROM  $table_name_config ", ARRAY_N );
        
        if($config[0]!=0){
			$varsEnv= $wpdb->get_row( "SELECT DISTINCT id,
                                          ws_uri_production_env,
                                          ws_uri_test_env,
                                          ws_r_regions,
                                          ws_r_sub_regions,
                                          ws_env_last_upgrade,

                                          ws_p_cgiar_research_programs,
                                          ws_p_management_team,
                                          ws_p_office_of_the_director_general,
                                          ws_p_research_staff,
                                          ws_p_research_support,
                                          ws_p_research_theme_and_program_leaders


                                   FROM   $table_name_config
                                   WHERE  ws_uri_production_env <> '' AND
                                          ws_uri_test_env <> '' ", ARRAY_N );
			
            runkit_constant_redefine('WP_DB_ROW_CONFIG_ID',$varsEnv[0]); 
            runkit_constant_redefine('WP_URI_TEST_',$varsEnv[1]);
            runkit_constant_redefine('WP_URI_PRODUCTION_',$varsEnv[2]);  
            runkit_constant_redefine('WP_REGIONS_STAFF_',$varsEnv[3]);  
            runkit_constant_redefine('WP_SUB_REGIONS_STAFF_',$varsEnv[4]);
            runkit_constant_redefine('WP_LAST_UPDATE_STAFF_',$varsEnv[5]);

            runkit_constant_redefine('WP_CGIAR_RESEARCH_PROGRAMS_',$varsEnv[6]);
            runkit_constant_redefine('WP_MANAGEMENT_TEAM_',$varsEnv[7]);
            runkit_constant_redefine('WP_OFFICE_OF_THE_DIRECTOR_GENERAL_',$varsEnv[8]);
            runkit_constant_redefine('WP_RESEARCH_STAFF_',$varsEnv[9]);
            runkit_constant_redefine('WP_RESEARCH_SUPPORT_',$varsEnv[10]);
            runkit_constant_redefine('WP_RESEARCH_THEME_AND_PROGRAM_LEADERS_',$varsEnv[11]);

            //UNIT TEST
            //$paisesStaff=new PaisesController();
            //$regionesStaff=new RegionesController();
            //var_dump(maybe_serialize($paisesStaff->paisesAll()));
            //var_dump(json_decode($paisesStaff->paisesAll(),true));
            
            //$paisSerializado=
            //runkit_constant_redefine('WP_PAISES_ALL',maybe_serialize(($paisesStaff->paisesAll())));
            //runkit_constant_redefine('WP_REGIONES_ALL',maybe_serialize($regionesStaff->regionesAll()));
            //var_dump(WP_PAISES_ALL);

		}
	}
	
    function allow_custom_host( $allow, $host, $url ) { return $allow;}
	
	public function allow_origin() 
    {
		header( 'X-UA-Compatible: IE=edge,chrome=1' );
	    session_cache_limiter(false);
	    header("Cache-Control: public, s-maxage=120");
	    header("Access-Control-Allow-Origin: *");
	    header('Content-type: application/xml');
	    header('Content-type: image/jpeg');
	    header('Content-type: image/jpg');
	    header('Content-type: image/png');
	    header('Content-type: image/gif');
	}
	
    public function  wp_staff_page_template()
    {
        if ( version_compare( floatval( get_bloginfo( 'version' ) ), '4.7', '<' ) ) {
			add_filter('page_attributes_dropdown_pages_args',array( $this, 'register_project_templates'));
		}else {
            add_filter('theme_page_templates',array( $this, 'add_new_template'));
        }
		
		add_filter('wp_insert_post_data', array( $this, 'register_project_templates'));
		
	    add_filter('template_include', array( $this, 'view_project_template'));
        
        $this->templates = array('page_general_staff.php'=>'Staff Template',
                                 'page_regions_staff.php'=>'Staff Regions Template',
                                 'page_topics_staff.php'=>'Staff Topics Template',
                                 'page_rarea_staff.php'=>'Staff Research Area Template');

    }

    public function add_new_template( $posts_templates ) {
        	$posts_templates = array_merge( $posts_templates, $this->templates );
        	return $posts_templates;
    }

    public function register_project_templates( $atts ) {
    
        $cache_key = 'page_templates-' . md5( get_theme_root() . '/' . get_stylesheet() );
		$templates = wp_get_theme()->get_page_templates();

		if ( empty( $templates ) ) {  $templates = array(); } 
		
		wp_cache_delete( $cache_key , 'themes');
		
		$templates = array_merge( $templates, $this->templates );
		
		wp_cache_add( $cache_key, $templates, 'themes', 1800 );
		return $atts;
	}

    public function view_project_template( $template ) {
        global $post;
        if ( ! $post ) { 
            return $template; 
        }
		
		if ( !isset( $this->templates[get_post_meta($post->ID, '_wp_page_template', true )] ) ) {
            return $template;
		 }
	    

        //$file = WP_PLUGIN_TEMPLATES.'custom_templates/page_general_staff.php';  

        $file = WP_PLUGIN_TEMPLATES.'custom_templates/'.get_post_meta( $post->ID, '_wp_page_template', true);


        if ( file_exists( $file ) ) {
			return $file;
		} else {
			echo $file;
		}
		 
		return $template;
	}

    public function view_regions_staff_template($template){

		global $post;

		if(!$post){ return $template; }

		if ( !isset( $this->templates[get_post_meta( $post->ID, '_wp_page_template', true )] ) ) {
			return $template;
		}

        $file = WP_PLUGIN_TEMPLATES.'custom_templates/page_regions_staff.php';

		if ( file_exists( $file ) ) {
			return $file;
		} else {
			echo $file;
		}

		return $template;
	}

    public function view_topics_staff_template($template){

		global $post;

		if(!$post){ return $template; }

		if ( !isset( $this->templates[get_post_meta( $post->ID, '_wp_page_template', true )] ) ) {
			return $template;
		}

        $file = WP_PLUGIN_TEMPLATES.'custom_templates/page_topics_staff.php';

		if ( file_exists( $file ) ) {
			return $file;
		} else {
			echo $file;
		}

		return $template;
	}

    public function view_researchArea_staff_template($template){

		global $post;

		if(!$post){ return $template; }

		if ( !isset( $this->templates[get_post_meta( $post->ID, '_wp_page_template', true )] ) ) {
			return $template;
		}

        $file = WP_PLUGIN_TEMPLATES.'custom_templates/page_rarea_staff.php';

		if ( file_exists( $file ) ) {
			return $file;
		} else {
			echo $file;
		}

		return $template;
	}

    public function taxonomies_up()
    {
        require_once (WP_PLUGIN_DIR_INC .'/taxonomies.php');
    }

    //[scPageStaff lang="languaje"] [/scPageStaff]
    public function loadSCPageStaff($atts,$content=null){
        extract(shortcode_atts(array("lang"=>'EN-en'), $atts, 'scPageStaff'));
        $lang=$atts['lang'];
        $assets=WP_PLUGIN_ASSETS;
        
        $pCGIARResearchPrograms=WP_CGIAR_RESEARCH_PROGRAMS_;
        $pManagementTeam=WP_MANAGEMENT_TEAM_;
        $pOffficeOfTheDirectorGeneral=WP_OFFICE_OF_THE_DIRECTOR_GENERAL_;
        $pResearchStaff=WP_RESEARCH_STAFF_;
        $pResearchSupport=WP_RESEARCH_SUPPORT_;
        $pResearchThemeAndProgamsLeaders=WP_RESEARCH_THEME_AND_PROGRAM_LEADERS_;

        if($lang=="EN-en"){
            require_once (WP_PLUGIN_TEMPLATES .'shortcodes/pageStaff.php');
        }else if($lang=="ES-es"){
            require_once (WP_PLUGIN_TEMPLATES .'shortcodes/pageStaff-ES-es.php');
        }
    }
    
    //[scPageStaffRegions region="region"] [/scPageStaffRegions]
    public function loadSCPageStaffRegions($atts,$content=null)
    {
       extract(shortcode_atts(array("region"=>'region',"subregions"=>'subregions'), $atts, 'scPageStaffRegions'));
       $region=$atts['region'];//africa-asia-Latin America and the Caribbean
       $subregion=$atts['subregion'];
       $title=$region.'Staff';
       $assets=WP_PLUGIN_ASSETS;
       //$outHtml=require_once (WP_PLUGIN_TEMPLATES .'shortcodes/pageStaffRegions.php');
       require_once (WP_PLUGIN_TEMPLATES .'shortcodes/pageStaffRegions.php');
       //return $outHtml;
    }

    //[scPageStaffRArea area="area"] [/scPageStaffRArea]
    public function loadSCPageStaffRArea($atts,$content=null)
    {
       extract(shortcode_atts(array("area"=>'area'), $atts, 'scPageStaffRArea'));
       $region=$atts['region'];
       $subregion=$atts['subregion'];
       $title=$region.'Staff';
       $assets=WP_PLUGIN_ASSETS;
       $area=$atts['area'];
       require_once (WP_PLUGIN_TEMPLATES .'shortcodes/pageStaffRArea.php');
    }

    //[scPageStaffTopics topic="topic"]  [/scPageStaffTopics]
    public function loadSCPageStaffTopics($atts,$content=null){
       extract(shortcode_atts(array("topic"=>'topic'), $atts, 'scPageStaffTopics')); 
       $region=$atts['region'];
       $subregion=$atts['subregion'];
       $title=$region.'Staff';
       $assets=WP_PLUGIN_ASSETS;
       $topic=$atts['topic'];
       require_once (WP_PLUGIN_TEMPLATES .'shortcodes/pageStaffTopics.php');
    }

    //[scPageProfileStaff][/scPageProfileStaff]
    public function loadSCProfileStaff($atts,$content=null){
        //require_once (WP_PLUGIN_TEMPLATES .'shortcodes/pageStaff.php');
        //$this->plugin_nonce=wp_create_nonce('m');
    }

    public function loadSCTabs($atts,$content=null){
        extract(shortcode_atts(array("tab"=>'tab'), $atts, 'scTabsStaff'));
        $outHtml='';
        if($atts['tab']==='bio'){
            require_once (WP__plugin_dir_template .'shortcodes/tabBio.php');
        }else if($atts['tab']==='blogpost'){
            require_once (WP__plugin_dir_template .'shortcodes/tabBlogPost.php');
        }else if($atts['tab']==='publications'){
            require_once (WP__plugin_dir_template .'shortcodes/tabPublications.php');
        }
        return $outHtml;
    }

    //[shortSearch cultive="bean"]
    public function shortSearchContent($atts,$content=null){
        extract(shortcode_atts(array("cultive"=>'cultive'), $atts, 'shortSearch'));
        $html='';
        if($atts['cultive']==='bean'){
            require_once (WP__plugin_dir_template .'shortcodes/shortCodeBean.php');
        }else if($atts['cultive']==='forage'){
            require_once (WP__plugin_dir_template .'shortcodes/shortCodeForage.php');
        }else if($atts['cultive']==='cassava'){
            require_once (WP__plugin_dir_template .'shortcodes/shortCodeCassava.php');
        }
        return $html;
    }

    public function install(){
        require_once (WP_PLUGIN_DIR_INC . 'install.php');
        file_put_contents(WP__plugin_dir.'/app/core/logs'.'/log-install.txt', ob_get_contents());
    }
    
    public function uninstall()
    {
        require_once (WP_PLUGIN_DIR_INC . 'uninstall.php');
        file_put_contents(WP__plugin_dir.'/app/core/logs'.'/log-uninstall.txt', ob_get_contents());
    }

    /*
     *
     *
     */
    public function styles_up()
    {
        require_once (WP_PLUGIN_DIR_INC . 'stylesup.php');
    }

    /*
     *
     *
     */
    public function scripts_up()
    {
        require_once (WP_PLUGIN_DIR_INC. 'scriptsup.php');
    }

    /*
     *
     *
     */
    public function add_admin_menu()
    {
        require_once (WP_PLUGIN_DIR_INC.'adminmenu.php');
    }

    /*
     *
     *
     */
    function sidebar_up()
    {
         require_once (WP_PLUGIN_DIR_INC.'sidebars.php');
    }

    public function init(){
        if (function_exists('wp_register_style') && function_exists('wp_enqueue_style')) {
          add_action( 'wp_enqueue_scripts', array( $this, 'styles_up' ) );
        }

        if(function_exists('wp_register_script')&&function_exists('wp_enqueue_script')){
          add_action( 'wp_enqueue_scripts', array($this,'scripts_up') );
        }


        if (function_exists('register_activation_hook') &&
            function_exists('register_deactivation_hook')) {

            register_activation_hook(__FILE__, array($this, 'install'));
            register_deactivation_hook(__FILE__, array($this, 'uninstall'));
            
        }

        /*if (function_exists('wp_register_script')
            && function_exists('wp_enqueue_script')) {
            $this->scripts_up();
        }*/

        if (function_exists('add_shortcode')) {
            add_shortcode('scPageStaff',array($this,'loadSCPageStaff'));
            add_shortcode('scPageStaffRegions',array($this,'loadSCPageStaffRegions'));
            add_shortcode('scPageStaffTopics',array($this,'loadSCPageStaffTopics'));
            add_shortcode('scPageStaffRArea',array($this,'loadSCPageStaffRArea'));
        }

        if(function_exists('add_action')){
            
            //$this->initEnvVars();
            /*$envTestStaff=(defined('WP_URI_TEST_')?WP_URI_TEST_:'');
            $envProductionStaff=defined('WP_URI_PRODUCTION_')?WP_URI_PRODUCTION_:'';
            $envRegionsStaff=(defined('WP_REGIONS_STAFF_')?json_encode(unserialize(WP_REGIONS_STAFF_)):'') ;*/
	        add_action( 'admin_menu', array($this,'add_admin_menu'));
            
           // add_action( 'widgets_init', array($this,'sidebar_up'));
        }

        if(function_exists('add_filter')){
            //add_filter( 'page_template', array($this,'wp_staff_page_template'));

            add_action( 'plugins_loaded', array($this, 'wp_staff_page_template' ) );
            //add_filter( 'page_template', array($this,'wp_staff_page_template_general'));
            //add_filter( 'page_template', array($this,'wp_staff_page_template_regions'));
        }

        if(function_exists('register_taxonomy')){
            $this->taxonomies_up();
        }

        add_action('init', array($this,'allow_origin') );

       $requestStaff=new StaffController();
       //$paisesStaff=new PaisesController();
       //$regionesStaff=new RegionesController();
       //var_dump($paisesStaff->paisesAll());
       // var_dump($paisesStaff->paisesByName("Col"));
      //var_dump($regionesStaff->regionesByName("Afr"));

       $this->initEnvVars();
       
    }

    
    function get_custom_field( $object, $field_name, $request ) {
        return get_post_meta( $object[ 'id' ], $field_name, true );
    }

    /*
     *
     *
     */
    public function purge_persistence()
    {   
        /**SELECT * FROM wp_ciat2016.wp_options WHERE wp_ciat2016.wp_options.option_name like '_transient%'
           SELECT * FROM snapshot_ciat2016.wp_options WHERE snapshot_ciat2016.wp_options.option_name like '_transient%'
        */
        //SELECT * FROM `wp_options` WHERE `option_name` LIKE ('%\_transient\_staff%');
        global $wpdb;

        $sql = "
            DELETE
            FROM wp_options
            WHERE option_name like '\_transient\_staff%'
            OR option_name like '\_transient\_timeout\_staff%'
        ";

        $wpdb->query($sql);
        //-------------------file_put_contents(WP__plugin_dir.'/app/core/logs'.'/log-staff.txt', ob_get_contents());
        /// $wpdb->options
        $sql = "
            DELETE
            FROM wp_options
            WHERE option_name like '\_transient\_carnet%'
            OR option_name like '\_transient\_timeout\_carnet%'
        ";

        $wpdb->query($sql);
        //-------------------file_put_contents(WP__plugin_dir.'/app/core/logs'.'/log-carnet.txt', ob_get_contents());

    }
    
    public function getPluginName() 
    {
        return $this->plugin_name;
    }
    
    public function setPluginName($plugin_name)
    {
        $this->plugin_name = $plugin_name;
    }

    public function getPluginUriPathTest() 
    {
        return $this->plugin_uri_path_test;
    }
    
    public function setPluginUriPathTest($plugin_uri_path_test)
    {
        $this->plugin_uri_path_test = $plugin_uri_path_test;
    }   

    public function getPluginNonce() 
    {
        return $this->plugin_nonce;
    }

    public function setPluginNonce($plugin_nonce)
    {
        $this->plugin_nonce = $plugin_nonce;
    }

}$plugin = new StaffCIAT();
