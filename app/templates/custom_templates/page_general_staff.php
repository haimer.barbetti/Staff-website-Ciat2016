<?php defined( 'ABSPATH' ) or die( 'Not Access!');
 /* Template Name: Staff Template */ 
get_header();
$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() ); ?>
<div id="main-content">
<?php if (!$is_page_builder_used) : ?>
	<div class="container">
		<div id="content-area" class="clearfix">
			<!--<div id="left-area">--> 
	<div id="left">
<?php endif; ?>

			<?php while (have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if ( ! $is_page_builder_used ) : ?>

				<?php
					$thumb = '';
					$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );
					$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
					$classtext = 'et_featured_image';
					$titletext = get_the_title();
					$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
					$thumb = $thumbnail["thumb"];

					if ( 'on' === et_get_option( 'divi_page_thumbnails', 'false' ) && '' !== $thumb )
						print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
				?>

				<?php endif; ?>

					<div class="entry-content">
					<?php
						the_content();
						if (!$is_page_builder_used)
							wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
					?>
					</div> 

				<?php
					if (!$is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );
				?>

				</article> 

			<?php endwhile; ?>



<?php if (!$is_page_builder_used ) : ?>

			</div> <!-- #left-area -->

		 <!-- <div class="container-left-sidebar"> --> 
			 
			 <div class="container-left-sidebar" v-show="!activeView">
			 
		 <div id="search-form-staff-general" v-show="!profileClicked">
			<!--<input v-model="search" type="text" placeholder="Enter a name" id="s" debounce="100" v-bind="exportExternalModel()" lazy>-->
			<input v-model="search" type="text" placeholder="Enter a name" id="s" debounce="12" v-bind="exportExternalModel()" >
			<input type="submit" id="searchsubmit" value="&#xf002;" class="btn fa-input">
			<br>
		</div>

<?php 
			
		$post_type = 'filter';
        $taxonomies = get_object_taxonomies( (object) array('post_type' =>$post_type ) );

		foreach($taxonomies as $taxonomy ) : 
			
			$terms = get_terms( $taxonomy );
			foreach($terms as $term ): 
				$posts = new WP_Query("taxonomy=$taxonomy&term=$term->slug&posts_per_page=-1&orderby=title&order=ASC&" );
				echo "<h4><b>". strtoupper("Filter By ".$term->name)."</b></h4>";
				echo '<div id="filters-'.$term->slug.'">';
				$tempitem=array();
				if( $posts->have_posts() ): 
				while( $posts->have_posts() ) : $posts->the_post(); 
					$json=the_title(' ',' ',false); 
				if($json!=null&&$json!=""): ?>
			    <script>
var json = '<?php echo $json; ?>';
</script> 
				<a href="#" @click.stop.prevent="setListItem('<?php echo $json; ?>')" href="javascript:void(0);"> 
				  <?php  the_title(); ?> 
				</a>
				<br>
				
				<?php 
				endif;

				endwhile; 
				
			  endif; 
			   ?>
			  
			  </div>

			  <hr/>
			  
              <?php
			 endforeach; 

        endforeach;
			
			?>
	
	</div>

		<script>
var json = '<?php echo $json; ?>';
</script>

		<div class="container-right-sidebar" v-show="activeView">
			
			<h6> Email: <br> 
			    <a href="{{ infoform.emailAddress }}"> {{ infoform.emailAddress | lowercase | capitalize }} </a>  </h6>
			<br>
			<h6 > Phone: <br> +57 (2) {{ infoform.telephone }} </h6>

<!--			<img class="social" src="<?php //echo WP_PLUGIN_ASSETS_PUBLIC_IMGES.'twitter-logo-24.png'; ?>" alt="social-twitter">
			<img class="social" src="<?php //echo WP_PLUGIN_ASSETS_PUBLIC_IMGES.'linkedin-logo-24.png'; ?>" alt="social-twitter">-->


			<div id="social" >
				
				<li class="et-social-icon et-social-twitter-inv" v-if="infoform.twitter!=null">
					<a href="https://twitter.com/{{ infoform.twitter | purifiesTwitterLink}}" class="icon" target="_blank"> 
						<span>Twitter</span>
					</a>
				</li>
						
				<li class="et-social-icon et-social-linkedin-inv" v-if="infoform.linkedInProfile!=null">
					<a href="{{infoform.linkedInProfile}}" class="icon" target="_blank">
						<span>Linkedin</span>
					</a>
 				</li>

			 </div>

			<hr/>

			<h2><strong> Research area or unit </strong></h2>
			<p>
			{{ infoform.researchArea | lowercase | capitalize }}</p>	

			<hr/>

			<h2><strong> Topics </strong></h2>
			
			<p>{{infoform.fieldsOfExpertise  | lowercase |  capitalize}} </p>
			<p>{{infoform.fieldsOfExpertise2  | lowercase |  capitalize}} </p>
			<p>{{infoform.fieldsOfExpertise3  | lowercase |  capitalize}} </p>
			<p>{{infoform.fieldsOfExpertise4  | lowercase |  capitalize}} </p>
			<p>{{infoform.fieldsOfExpertise5  | lowercase |  capitalize}} </p>
				

		<div id="return" v-show="activeView">
		
			<div class="et_pb_button_module_wrapper et_pb_module et_pb_button_alignment_right">
					
					<a class="et_btn_back et_pb_button et_pb_custom_button_icon  et_pb_button_0 et_pb_module et_pb_bg_layout_light" 
					href="javascript:void(0);" 
					@click.stop.prevent="clickBack()"
					data-icon="4">Back</a>

			</div>
		
		</div>	


		</div>	
		
		<!--<div id="return" v-show="activeView" class="et_pb_button_module_wrapper et_pb_module">
			<h6> <a href="#" @click.stop.prevent="clickBack()">&nbsp;←  Back </a>  </h6>	
			<a href="#" class="et_pb_button  et_pb_button_0 et_pb_module et_pb_bg_layout_light" 
			   @click.stop.prevent="clickBack()">Back </a>

		</div> -->

		<!--<div id="return" v-show="activeView">
		
			<div class="et_pb_button_module_wrapper et_pb_module et_pb_button_alignment_right">
					
					<a class="et_btn_back et_pb_button et_pb_custom_button_icon  et_pb_button_0 et_pb_module et_pb_bg_layout_light" 
					href="javascript:void(0);" 
					@click.stop.prevent="clickBack()"
					data-icon="4">Back</a>

			</div>
		
		</div>-->

		</div> <!-- #content-area-general -->
	</div> <!-- .container -->

<?php endif; ?>



<script>

//Modelo template general

var m = new Vue({
	el: '#content-area',
	data: {
		personArray: [],
		infoform: [],
		infoemail: "",
		infophone: "",
		activeView: false,
		search: "",
		itemSelected: "",
		jsonSelectTemp: ""
	},

	ready: function () {

		this.$set('jsonSelectTemp', json);


	},

	computed: {},

	methods: {

		setPage: function (pageNumber) {
			this.currentPage = pageNumber;
		},
		setListItem: function (itemSelected) {

			myViewModel.resetFilters();

			console.log(" Change selected Item " + itemSelected);
			this.$set('itemSelected', itemSelected);
			
			myViewModel.$set('search', "");
			

			if (itemSelected == " Asia " || itemSelected == " Africa " ||
				itemSelected == " Headquarters " || itemSelected == " Americas (not including HQ) " ||
				itemSelected == " Other ") {

				console.log(" Region ");
				if (itemSelected == " Other ") {
					myViewModel.regionClick = "";
				} else {
					myViewModel.regionClick = itemSelected;
				}

			} else {
				console.log("No Region ");
				myViewModel.regionClick = "";
			}

			if (itemSelected == " Agrobiodiversity " || itemSelected == " Soils and Landscapes for Sustainability " ||
				itemSelected == " Decision and Policy Analysis (DAPA) ") {

				console.log(" researchArea");
				myViewModel.researchAreaClick = itemSelected;

			} else {
				console.log("No researchArea");
				//myViewModel.regionClick="";
				myViewModel.researchAreaClick = "";
			}


			for (t in myViewModel.listTopics) {

				if (myViewModel.listTopics[t].name.trim() === itemSelected.trim()) {
					console.log(myViewModel.listTopics[t].name + " FILTRARIA " + itemSelected);
					myViewModel.topicsClick = itemSelected.trim();
				} else {

				}
			}

			//MT = Management Team
			var arrayGroupMT = ["CGIAR Research Programs",
				"Management Team",
				"Office Of The Director General",
				"Research Staff", "Research Support",
				"Research Theme And Program Leaders"];

			for (index in arrayGroupMT) {
				console.log(arrayGroupMT[index] + " " + itemSelected.trim());
				if (arrayGroupMT[index].toLowerCase() == itemSelected.trim().toLowerCase()) {
					console.log(arrayGroupMT[index] + " FILTRARIA " + itemSelected);
					myViewModel.groupSelectedMTClick = itemSelected.trim();
				}
			}

		},
		searchLetter: function (person) {

			return person.firstName.toLowerCase().indexOf(this.letter.toLowerCase()) != -1 ||
				person.lastName.toLowerCase().indexOf(this.letter.toLowerCase()) != -1;

		},
		exportExternalModel: function () {
			myViewModel.resetFilters();
			myViewModel.$set('search', this.search);
		},
		searchInputText: function (person) {

			return person.firstName.toLowerCase().indexOf(this.search.toLowerCase()) != -1 ||
				   person.lastName.toLowerCase().indexOf(this.search.toLowerCase()) != -1;

		},
		getInfo: function (person) {
			this.$set('personArray', person);
			this.setInfo();
		},
		setInfo: function () {
			this.$set('infoform', this.personArray);
			this.$set('activeView', true);
		},
		showView: function () {
			this.$set('activeView', true);
		},
		clickBack: function () {
			myViewModel.$set('profileClicked', false);
			this.$set('activeView', false);
			jQuery("html, body").animate({ scrollTop: 10 + "px" });


			jQuery('html,body').animate({
				scrollTop: jQuery("#content-area").offset().top
			}, 1500);

		}

	},

	filters: {
		purifiesTwitterLink: function (strTwitter) {
			var str = "";

			if (strTwitter != "" && strTwitter != null && strTwitter != undefined) {

				if (strTwitter.indexOf('@') != -1) {
					str = strTwitter.substr(1);
					return str;
				} else if (strTwitter.indexOf('@') == -1) {
					if (strTwitter != "" && strTwitter != undefined) {
						return strTwitter;
					} else {
						return "";
					}
				} else if (this.isUrl(strTwitter)) {
					str = strTwitter.split("/")[3];
					return str;
				} else {
					return "";
				}
			}
		}
	}



});


</script>

<script>
			jQuery(document).ready(function () {

				/*jQuery('.publications').load(function () {
					jQuery('.navbar-static-top').find('.container').remove();
					jQuery('body').find('header').remove();
					jQuery('.trail-wrapper').remove();
					jQuery('.ds-div-head.page-header.first-page-header').remove();
					jQuery('#aspect_discovery_SimpleSearch_div_discovery-search-box').remove();
					jQuery('#aspect_discovery_SimpleSearch_div_main-form').remove();
					jQuery('.pagination-masked.clearfix.top').remove();
				});*/

			});
</script>
</div> 

<?php get_footer(); ?>