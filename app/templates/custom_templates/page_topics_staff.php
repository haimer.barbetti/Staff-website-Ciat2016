<?php defined( 'ABSPATH' ) or die( 'Not Access!');
 /* Template Name: Staff Topics Template */ 
get_header();
$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );
?>

<div id="main-content">
<?php  if (!$is_page_builder_used): ?>
	
	<div class="container">
		<div id="content-area" class="clearfix">
			<!--<div id="left-area">--> 
			<div id="left">

<?php elseif($is_page_builder_used): ?>


			<div id="content-area" >
				
<?php endif; ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >

				<?php if ( ! $is_page_builder_used ) : ?>

					
				<?php
					$thumb = '';

					$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );

					$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
					$classtext = 'et_featured_image';
					$titletext = get_the_title();
					$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
					$thumb = $thumbnail["thumb"];

					if ( 'on' === et_get_option( 'divi_page_thumbnails', 'false' ) && '' !== $thumb )
						print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
				?>

				<?php endif; ?>



					<div class="entry-content">
					<?php
						the_content();
						//echo "despues del the_content()"; ?>

						 
	


<div class="container-right-sidebar-regions" v-show="activeView">

			<h6> Email <br> 
			    <a href="{{ infoform.emailAddress }}"> {{ infoform.emailAddress | lowercase | capitalize }} </a>  </h6>
			<br>
			<h6 > Phone <br> +57 (2) {{ infoform.telephone }} </h6>

			<div id="social" >
				<li class="et-social-icon et-social-twitter-inv" v-if="infoform.twitter!=null">
					<a href="https://twitter.com" class="icon" target="_blank"> 
						<span>Twitter</span>
					</a>
				</li>
						
				<li class="et-social-icon et-social-linkedin-inv" v-if="infoform.linkedInProfile!=null">
					<a href="https://www.linkedin.com/company/ciat" class="icon" target="_blank">
						<span>Linkedin</span>
					</a>
 				</li>
			 </div>

			<hr/>
			<h2><strong> Research Area </strong></h2>
			<p>{{ infoform.researchArea | lowercase | capitalize }}</p>	

			<hr/>

			<h2><strong> Topic </strong></h2>
			
			<p>
				{{infoform.fieldsOfExpertise  | lowercase |  capitalize}}
				{{infoform.fieldsOfExpertise2 | lowercase | capitalize}}
				{{infoform.fieldsOfExpertise3 | lowercase | capitalize}}
				{{infoform.fieldsOfExpertise4 | lowercase | capitalize}}
				{{infoform.fieldsOfExpertise5 | lowercase | capitalize}}
			</p>	

			<div id="return" v-show="activeView"
		<div class="et_pb_button_module_wrapper et_pb_module et_pb_button_alignment_right">
				<a class="et_btn_back et_pb_button et_pb_custom_button_icon  et_pb_button_0 et_pb_module et_pb_bg_layout_light" 
				   href="javascript:void(0);" 
				   @click.stop.prevent="clickBack()"
				   data-icon="4">Back</a>
	    </div>
		</div>
		</div>	
		
		<!--<div id="return" v-show="activeView">
			<h6> <a href="#" @click.stop.prevent="clickBack()">&nbsp;← Back </a>   </h6>	
		</div> -->

	<!--	<div id="return" v-show="activeView"
		<div class="et_pb_button_module_wrapper et_pb_module et_pb_button_alignment_right">
				<a class="et_btn_back et_pb_button et_pb_custom_button_icon  et_pb_button_0 et_pb_module et_pb_bg_layout_light" 
				   href="javascript:void(0);" 
				   @click.stop.prevent="clickBack()"
				   data-icon="4">Back</a>
	    </div>
		</div>-->


		</div> <!-- #content-area -->
	</div> <!-- .container -->


<script>



var	modelTopicsIn=new Vue({ 
			el: '#content-area', 
			data:{
				personArray:[],
				infoform:[],
				infoemail:"",
				infophone:"",
				activeView:false,
				search:""
			},
			
			ready: function() { },

			computed: { },

			methods:{
			  exportExternalModel:function(){
				  myViewModelTopicsOut.$set('search',this.search);
			  },
			  searchInputText: function(person) {
				return person.firstName.toLowerCase().indexOf(this.search.toLowerCase()) != -1|| 
					   person.lastName.toLowerCase().indexOf(this.search.toLowerCase()) != -1;
			  },
			  getInfo:function(person){
				  this.$set('personArray',person);
				  this.setInfo();
			  },
			  setInfo:function(){
				  this.$set('infoform',this.personArray);
				  this.$set('activeView',true);
			  },
			  showView:function(){
				 this.$set('activeView',true);
			  },
			  clickBack:function(){
				  myViewModelTopicsOut.$set('profileClicked',false);
				  this.$set('activeView',false);

				  /*jQuery('html,body').animate({
    				scrollTop: jQuery("#container-sc").offset().top
				}, 1500);*/
			  }

			}

		});

</script>


</div>


<?php 

						if (!$is_page_builder_used)
							wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
					?>

					
					</div> <!-- .entry-content -->

				<?php
					if (!$is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );
				?>

				</article> <!-- .et_pb_post -->

			<?php endwhile; ?>



<?php if (!$is_page_builder_used): ?>

</div>

<?php endif; ?>





<?php get_footer(); ?>