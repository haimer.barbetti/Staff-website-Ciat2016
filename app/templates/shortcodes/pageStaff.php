<?php defined('ABSPATH') or die( 'Not Access!'); ?>
<?php
if( defined('WP_CGIAR_RESEARCH_PROGRAMS_')&&defined('WP_MANAGEMENT_TEAM_')&&
    defined('WP_OFFICE_OF_THE_DIRECTOR_GENERAL_')&&defined('WP_RESEARCH_STAFF_')&&
    defined('WP_RESEARCH_SUPPORT_')&&defined('WP_RESEARCH_THEME_AND_PROGRAM_LEADERS_') ): ?>
<script>
var cRPrograms = '<?php echo json_encode(unserialize(WP_CGIAR_RESEARCH_PROGRAMS_)); ?>';
var mTeam = '<?php echo json_encode(unserialize(WP_MANAGEMENT_TEAM_)); ?>';
var oOTDirectorGeneral = '<?php echo json_encode(unserialize(WP_OFFICE_OF_THE_DIRECTOR_GENERAL_)); ?>';
var rStaff = '<?php echo json_encode(unserialize(WP_RESEARCH_STAFF_)); ?>';
var rSupport = '<?php echo json_encode(unserialize(WP_RESEARCH_SUPPORT_)); ?>';
var rTAProgramLeaders = '<?php echo json_encode(unserialize(WP_RESEARCH_THEME_AND_PROGRAM_LEADERS_)); ?>';
</script>
<?php endif; ?>
<?php if($assets!==''): ?>
<script>
var ass = '<?php echo $assets; ?>';
</script>
<?php endif; ?>

<?php if( defined('WP_REGIONS_STAFF_')&&defined('WP_SUB_REGIONS_STAFF_') ): ?>
<script>
	var envRegions = '<?php echo json_encode(unserialize(WP_REGIONS_STAFF_)); ?>';
	var envSubRegions = '<?php echo json_encode(unserialize(WP_SUB_REGIONS_STAFF_)); ?>';
</script>
<?php endif; ?>


<div id="my_view">
	<h1 id="title-staff" v-show="!profileClicked">
		<?php echo get_the_title(); ?>
	</h1>

	<div id="select-filter" name="filter-full" v-show="!profileClicked">
		<select v-model="categories" @change="handlerClickCategory()">
		<option value="">Select a Category</option>
		<option  v-for="list in listCategories" v-bind:value="list.name | lowercase | capitalize">{{ list.name }}</option>		
		</select>
	</div>



	<div id="filterLetter" v-show="!profileClicked">
		<ul id="letters">
			<li v-for="letter in letters" @click="setLetter(letter)">
				<a id="searchsubmit" href="javascript:void(0);">
					<h6><b>{{letter.letter}}</b>&nbsp;</h6>
				</a>
			</li>
		</ul>
	</div>

	<br>

	<div class="equalHMVWrap eqWrap" v-show="!profileClicked">

		<div class="equalHMV eq contenedor" v-for="person in persons | filterBy searchInputText | filterBy managementTeamFilterStart  | filterBy topicsFilterStart | filterBy researchAreaFilterStart | filterBy regionFilterStart  | filterBy searchLetter | paginate">
		
			<a href="#" class="link" @click.stop.prevent="handlerClicProfile(person)">   	

				
				{{erase()}}
				{{getImgCarnet(person.carnet)}}
				
				<div  v-for="photo in photosTemp" id="img-box-{{photo.carnet}}"  v-if="photo.carnet==person.carnet" track-by="_uidsdiv">
					
					<img  :src="assets"  v-img="photo.photo" v-if="photo.photo.length>26">

					<img  :src="'http://ciat2016.staging.wpengine.com/wp-content/plugins/StaffCIAT/images/image-not-found.png'"  
					v-img="photo.photo" v-if="photo.photo.length<=26">

				</div>
			
			<div id="legend" >

				<h4>
					{{person.firstName | concat person.lastName  | lowercase | capitalize }}
				</h4>

				<h5>
					{{person.position | limitChartsBy 22 | lowercase | capitalize}}
				</h5>
				<h6 >{{person.researchArea | lowercase | capitalize}}</h6>
			</div>	

			<div id="social" >

			<li class="et-social-icon et-social-twitter-staff" v-if="person.twitter!=null">
					<a href="https://twitter.com/{{ person.twitter | purifiesTwitterLink }}" class="icon" target="_blank"> 
						<span>Twitter</span>
					</a>
			</li>

			<li class="et-social-icon et-social-linkedin-staff" v-if="person.linkedInProfile!=null">
				<a href="{{person.linkedInProfile}}" class="icon" target="_blank">
					<span>Linkedin</span>
				</a>
				<!--<a href="https://www.linkedin.com/company/ciat" class="icon" target="_blank">
					<span>Linkedin</span>
				</a>-->
			</li>

		</div>
		</a>
	</div>
	<!--<div id="filterNumbers" v-show="!profileClicked">
		<ul id="numbers" class="pagination justify-content-end">
			<li class="page-item" v-for="pageNumber in totalPages" v-if="Math.abs(pageNumber - currentPage) < 3 || pageNumber == totalPages - 1 || pageNumber == 0">
				<a id="searchsubmit" href="#" @click="setPage(pageNumber)" :class="{current: currentPage === pageNumber, last: (pageNumber == totalPages - 1 && Math.abs(pageNumber - currentPage) > 3), first:(pageNumber == 0 && Math.abs(pageNumber - currentPage) > 3) }">
						{{ pageNumber+1 }}
				 </a>
			</li>
		</ul>
	</div>-->
</div>

<div id="filterNumbers" v-show="!profileClicked">
		<ul id="numbers" class="pagination justify-content-end">
			<li class="page-item" v-for="pageNumber in totalPages" v-if="Math.abs(pageNumber - currentPage) < 3 || pageNumber == totalPages - 1 || pageNumber == 0">
				<a id="searchsubmit" href="javascript:void(0);" @click.stop.prevent="setPage(pageNumber)" :class="{current: currentPage === pageNumber, last: (pageNumber == totalPages - 1 && Math.abs(pageNumber - currentPage) > 3), first:(pageNumber == 0 && Math.abs(pageNumber - currentPage) > 3) }">
						{{ pageNumber+1 }}
				 </a>
			</li>
		</ul>
</div>


<div id="profile-header" v-show="profileClicked">
	<h2 id="title-profile" v-show="profileClicked">
		<strong> {{ personClick.firstName | lowercase | capitalize }}&nbsp;{{personClick.lastName | lowercase | capitalize}}</strong>
	</h2>
	<br>
	<h3 id="sub-title-profile" v-show="profileClicked">
		<br> {{personClick.position | lowercase | capitalize}}
	</h3>

	<div class="box-img" v-for="photo in photosTemp" v-if="photo.carnet==personClick.carnet" track-by="_uidpdiv">

		<imagen v-show="profileClicked" :url="photo.photo" :id="photo.carnet" v-if="photo.photo.length>26">
		</imagen>

		<imagen v-show="profileClicked" :url="'http://ciat2016.staging.wpengine.com/wp-content/uploads/image-not-found.png'" :id="photo.carnet"
			v-if="photo.photo.length<=26">
		</imagen>

	</div>

	<div class="et_pb_module et_pb_tabs et_pb_tabs_0 container-tabs-custom" v-show="profileClicked">

		<ul class="et_pb_tabs_controls clearfix custom-tabs">
			<li class="et_pb_tab_0 et_pb_tab_active">
				<a id="tabs-staff">Bio</a>
			</li>
			<li class="et_pb_tab_1">
				<a id="tabs-staff">Blog posts</a>
			</li>
			<li class="et_pb_tab_1">
				<a id="tabs-staff">Publications</a>
			</li>
		</ul>

		<div class="et_pb_all_tabs">
			
			<div class="description et_pb_tab clearfix et_pb_active_content et_pb_tab_0 et-pb-active-slide">
				<h6>Description of work:</h6>
				<p>{{personClick.descriptionOfWork  }}</p>
				<h6>Language(s):</h6>
				<p>{{personClick.languages | lowercase | capitalizeFirstLetter }}</p>
				<h6>Location:</h6>
				<p>{{personClick.location | concat personClick.region  | capitalize }} </p>
			</div>
			
			<div class="et_pb_tab clearfix et_pb_tab_1">
				<p v-if="personClick.ciatBlogProfile!=null">
				<iframe src="http://blog.ciat.cgiar.org/author/{{personClick.ciatBlogProfile}}" class="publications" id="publications" height="400px" width="400px" frameborder="0">

				</iframe>
				</p>
				<p v-if="personClick.ciatBlogProfile==null">
					Esta persona no tiene publicaciones en Blog de CIAT, para consultar otros autores 
					consulte  <a src="http://blog.ciat.cgiar.org" target="_blank">blog.ciat.cgiar.org</a>
				</p>
				
			</div>
			
			<div class="et_pb_tab clearfix et_pb_tab_2">

				<iframe src="{{personClick.publications}}" class="publications" id="publications" height="400px" width="400px" frameborder="0">

				</iframe>
			</div>
			
		</div>
	</div>

</div>
<div>
</div>
</div>

<script charset="UTF-8">

	//var himalaya = require('himalaya');
	/*var html = require('fs').readFileSync('/webpage.html');
	var json = himalaya.parse(html);*/
	//var x2js = new X2JS();

	//var uri="http://ciat2016.wpengine.com/wp-admin/admin-ajax.php";
	//var uri = "http://ciat2016.staging.wpengine.com/wp-admin/admin-ajax.php";
	var uri = "http://ciat.cgiar.org/wp-admin/admin-ajax.php"
	Vue.config.debug = true;
	Vue.config.silent = true;
	//Vue.config.async = false;
	Vue.config.devtools = true;
	Vue.http.options.emulateJSON = true;
	Vue.http.options.emulateHTTP = true;


	/*Vue.http.interceptors.push((request, next) => {
		request.url += (request.url.indexOf('?') > 0 ? '&' : '?') + `cb=${new Date().getTime()}`
		next();
	});*/

	/*
			   var cache = new Cache({expiration: 300});
	
			   Vue.http.interceptors.push(function(request,next){
				
				if(request.method.toLowerCase()=='get'){
	
					//JSON.parse(cache.get("CACHE_http://ciat2016.staging.wpengine.com/wp-admin/admin-ajax.php?action=request_action_getPhotoStaff&lc=02875")).success;
	
					var found=cache.get("CACHE_"+request.url);
					console.log(found);
	
					if(found){
						console.log(" Hey Esta en Cache");
						next(request.respondWith(cache, {status: 200, statusText: 'Ok'}));
					}else{
						console.log(" Hey NO Esta en Cache");
					}
	
				}
				next(function(response){
					let {status, statusText, body} = response;
	
					if (status === 200 && request.method.toLowerCase() === 'get') {
						console.log(" Save Cache ");
						cache.set(`CACHE_${request.url}`, JSON.stringify(body));
					}
	
					setTimeout(request.respondWith(body, {status, statusText}));
	
				});
	
			   });*/

	/*Vue.http.interceptors.push({

	request: function (req) {
	if(req.method.toLowerCase() == 'get') {
		var found = cache.get(req.url);
		if(found) {
		req.client = function(){
			return found;
		}
		}
	}
	return req;
	}

	 });*/


	Vue.filter('concat', function (value, key) {
		return value + " " + key;
	});

	Vue.directive('img', function (url) {
		var img = new Image();
		img.src = url;
		img.onload = function () {
			this.el.src = url;

			jQuery(this.el)
				.css('opacity', 0.1)
				.addClass('img-responsive')
				.animate({ opacity: 1 }, 200)//311
		}.bind(this);
	});

	var $elem = jQuery(".container-left-sidebar");

	var imagen = Vue.extend({
		props: ['url', 'id'],
		template: '<img :src="url"  id="img-{{id}}" alt="img-{{id}}-alt">'
	});


	var myViewModel = new Vue({
		el: '#my_view',
		components: {
			imagen: imagen
		},
		data: {
			persons: [{
				location: "",
				position: "",
				carnet: 0,
				region: "",
				telephone: "",
				firstName: "",
				lastName: "",
				researchArea: "",
				languages: "",
				publications: "",
				descriptionOfWork: "",
				fieldsOfExpertise: "",
				emailAddress: "",
				ciatBlogProfile: "",
				twitter: "",
				linkedInProfile: "",
				researchGateProfile: "",
			}],
			letters: [{ letter: "All" }, { letter: "A" }, { letter: "B" }, { letter: "C" }, { letter: "D" }, { letter: "F" }, { letter: "G" },
			{ letter: "H" }, { letter: "I" }, { letter: "J" }, { letter: "K" }, { letter: "L" }, { letter: "M" },
			{ letter: "N" }, { letter: "O" }, { letter: "P" }, { letter: "Q" }, { letter: "R" }, { letter: "S" },
			{ letter: "T" }, { letter: "U" }, { letter: "V" }, { letter: "W" }, { letter: "X" }, { letter: "Y" },
			{ letter: "Z" }],
			search: "",
			selectedLetter: "",
			title: "Staff",

			currentPage: 1,
			itemsPerPage: 15,
			resultCount: 0,

			str: "",
			selected: "",
			filters: [],
			letter: "",
			categories: "",
			listCategories: [],
			profileClicked: false,
			left: $elem,
			rigth: jQuery(""),
			personClick: [],
			infoemail: "",
			staffCarnet: [{ carnet: 0, photo: "" }],
			photosTemp: [{ carnet: "", photo: "", _uidpdiv: "", _uidsdiv: "", _uidpimg: "", _uidsimg: "" }],
			tempCarnet: [{ carnet: 0 }],
			strNCarnets: "",
			photoTest: "",
			assets: "",
			default: "http://ciat2016.staging.wpengine.com/wp-content/uploads/image-not-found.png",
			eSubRegions: "",
			eRegions: "",

			pCgiarResearchPrograms: [],
			pManagementTeam: [],
			pOfficeOfTeDirectoGeneral: [],
			pResearchStaff: [],
			pResearchSupport: [],
			pResearchThemeAndProgramLeaders: [],

			groupSelectedMTClick: "",

			regionClick: "",
			researchAreaClick: "",
			topicsClick: "",
			positionsClick: "",

			/*ResearchStaffClick:"",
			ResearchThemeAndProgramLeadersClick:"",*/

			topics: "",
			positions: "",
			listTopics: [],
			listPositions: []
		},

		ready: function () {
			this.$set('eRegions', JSON.parse(envRegions));
			this.$set('eSubRegions', JSON.parse(envSubRegions));


			this.$set('pCgiarResearchPrograms', JSON.parse(cRPrograms));
			this.$set('pManagementTeam', JSON.parse(mTeam));
			this.$set('pOfficeOfTeDirectoGeneral', JSON.parse(oOTDirectorGeneral));
			this.$set('pResearchStaff', JSON.parse(rStaff));
			this.$set('pResearchSupport', JSON.parse(rSupport));
			this.$set('pResearchThemeAndProgramLeaders', JSON.parse(rTAProgramLeaders));




			this.getListCategories();
			this.getListTopics();
			this.getListPositions();
			this.getAllStaff();
			//this.getAllAuthors();
			this.$set('assets', ass + 'images/loader.gif');

		},
		computed: {

			totalPages: function () {
				//console.log(Math.ceil(this.resultCount / this.itemsPerPage) + "totalPages");
				return Math.ceil(this.resultCount / this.itemsPerPage);
			}/*,
				  concatenate: function (value, key) {
					  return value + this[key];
				  }*/

		},
		methods: {
			list: function (arr) {
				var out = "";
				for (obj in arr) {
					out += JSON.stringify(arr[obj]);
				}
				return out;
			},
			regionById: function (regionId, arr) {
				var regionOut = {};
				for (r in arr) {
					if (arr[r].r_id === regionId) {
						regionOut = arr[r];
					}
				}
				return regionOut;
			},

			regionByName: function (name) {
				var regionOut = {};
				var tempName = (name === undefined || name === null) ? "&&_" : name.toLowerCase();

				for (r in this.eRegions) {

					//if(this.eRegions[r].r_name.toLowerCase().indexOf( tempName )!=-1){
					if (tempName.includes(this.eRegions[r].r_name.toLowerCase())) {
						regionOut = this.eRegions[r];
					}
				}
				return regionOut;
			},

			subRegionByName: function (name) {
				var regionOut = {};

				var tempName = (name === undefined || name === null) ? "&&_" : name.toLowerCase();

				for (r in this.eSubRegions) {

					if (tempName.includes(this.eSubRegions[r].r_sub_region_name.toLowerCase())) {
						regionOut = this.eSubRegions[r];
					}

				}

				return regionOut;

			},

			regionBelongsToSubregion: function (regionStr, subRegionStr) {
				var flag = false;
				var region = this.regionByName(regionStr);
				var subRegion = this.subRegionByName(subRegionStr);

				if (region.r_id === subRegion.r_regions_r_id) {

					flag = true;
				}
				return flag;
			},
			regionFilterStart: function (person) {

				var verificador = this.regionBelongsToSubregion(this.regionClick, person.region);

				if (this.regionClick == "") {

					return this.persons;

				} else if (this.regionClick == " Asia ") {

					if (verificador) {
						this.regionClick = " Asia ";
						return person;
					} else {
						this.regionClick = " Asia ";
						return null;
					}

				} else if (this.regionClick == " Africa ") {

					if (verificador) {
						this.regionClick = " Africa ";
						return person;
					} else {
						this.regionClick = " Africa ";
						return null;
					}

				} else if (this.regionClick == " Americas (not including HQ) ") {

					if ((verificador) && (this.regionClick == " Americas (not including HQ) ") && (person.location != "HQ")) {
						this.regionClick = " Americas (not including HQ) ";
						return person;

					} else {
						this.regionClick = " Americas (not including HQ) ";
						return null;
					}

				} else if (this.regionClick == " Headquarters " && person.location == "HQ") {

					return person;

				} else {
					return null;
				}
			},

			researchAreaFilterStart: function (person) {

				if (this.researchAreaClick == "") {
					return this.persons;
				} else if (this.researchAreaClick == " Agrobiodiversity ") {
					if (person.researchArea == "AGBIO OTHERS") {
						this.researchAreaClick = " Agrobiodiversity ";
						return person;
					} else {
						this.researchAreaClick = " Agrobiodiversity ";
						return null;
					}
				} else if (this.researchAreaClick == " Soils and Landscapes for Sustainability ") {

					if (person.researchArea == "SOILS OTHERS") {
						this.researchAreaClick = " Soils and Landscapes for Sustainability ";
						return person;
					} else {
						this.researchAreaClick = " Soils and Landscapes for Sustainability ";
						return null;
					}
				} else if (this.researchAreaClick == " Decision and Policy Analysis (DAPA) ") {

					if (person.researchArea == "DAPA OTHERS") {
						this.researchAreaClick = " Decision and Policy Analysis (DAPA) ";
						return person;
					} else {
						this.researchAreaClick = " Decision and Policy Analysis (DAPA) ";
						return null;
					}

				} else {
					return null;
				}
			},

			topicsFilterStart: function (person) {

				var topic1 = (person.fieldsOfExpertise != null && person.fieldsOfExpertise != undefined) ? person.fieldsOfExpertise.trim() : "";
				var topic2 = (person.fieldsOfExpertise2 != null && person.fieldsOfExpertise2 != undefined) ? person.fieldsOfExpertise2.trim() : "";
				var topic3 = (person.fieldsOfExpertise3 != null && person.fieldsOfExpertise3 != undefined) ? person.fieldsOfExpertise3.trim() : "";
				var topic4 = (person.fieldsOfExpertise4 != null && person.fieldsOfExpertise4 != undefined) ? person.fieldsOfExpertise4.trim() : "";
				var topic5 = (person.fieldsOfExpertise5 != null && person.fieldsOfExpertise5 != undefined) ? person.fieldsOfExpertise5.trim() : "";
				var researchArea = (person.researchArea != null) ? person.researchArea.toLowerCase() : "";

				//UNIDTARIES TEST
				console.log(" PRE FILTRADO \n Topic1: " + topic1 + "\n  Topic2: " + topic2 + "\n " +
					" Topic3: " + topic3 + "\n Topic4: " + topic4 + "\n Topic5: " + topic5 + "\n Topic Sel:" + this.topicsClick + "\n AREA: " +
					person.researchArea);

				if (this.topicsClick == "") {
					return this.persons;
				} else if (topic1.toLowerCase() == this.topicsClick.toLowerCase() ||
					topic2.toLowerCase() == this.topicsClick.toLowerCase() ||
					topic3.toLowerCase() == this.topicsClick.toLowerCase() ||
					topic4.toLowerCase() == this.topicsClick.toLowerCase() ||
					topic5.toLowerCase() == this.topicsClick.toLowerCase() ||
					researchArea.includes(this.topicsClick.toLowerCase())
				) {
					console.log(" FILTRADO " + researchArea + " " + this.topicsClick.toLowerCase());
					//-console.log(" FILTRADO "+person.fieldsOfExpertise.trim()+" "+this.topicsClick);
					return person;
				} else {
					return null;
				}

			},

			managementTeamFilterStart: function (person) {
				var position = (person.position != null && person.position != undefined) ? person.position.trim() : "";
				var arrayGroupMTFilter = ["CGIAR Research Programs", "Management Team", "Office Of The Director General",
					                      "Research Staff", "Research Support", "Research Theme And Program Leaders"];

				if (this.groupSelectedMTClick == "") {
					return this.persons;
				} else if (this.groupSelectedMTClick.toLowerCase() == arrayGroupMTFilter[0].toLowerCase()) {
					for (p in this.pCgiarResearchPrograms) {

					}
				} else if (this.groupSelectedMTClick.toLowerCase() == arrayGroupMTFilter[1].toLowerCase()) {
					for (pMT in this.pManagementTeam) {
						if (this.pManagementTeam[pMT].p_management_team == position) {
							return person;
						}
					}
				} else if (this.groupSelectedMTClick.toLowerCase() == arrayGroupMTFilter[2].toLowerCase()) {
					for (pOOTDG in this.pOfficeOfTeDirectoGeneral) {
						if (this.pOfficeOfTeDirectoGeneral[pOOTDG].p_office_of_director_general == position) {
							return person;
						}
					}
				} else if (this.groupSelectedMTClick.toLowerCase() == arrayGroupMTFilter[3].toLowerCase()) {
					for (pRStaff in this.pResearchStaff) {
						if (this.pResearchStaff[pRStaff].p_research_staff == position) {
							return person;
						}
					}
				} else if (this.groupSelectedMTClick.toLowerCase() == arrayGroupMTFilter[4].toLowerCase()) {
					for (pRS in this.pResearchSupport) {
						if (this.pResearchSupport[pRS].p_research_support == position) {
							return person;
						}
					}
				} else if (this.groupSelectedMTClick.toLowerCase() == arrayGroupMTFilter[5].toLowerCase()) {
					for (pRTP in this.pResearchThemeAndProgramLeaders) {
						if (this.pResearchThemeAndProgramLeaders[pRTP].p_research_theme_and_program_leaders == position) {
							return person;
						}
					}
				} else {
					return null;
				}


			},
			resetFilters: function () {
				//-this.regionClick = "";
				//-this.topicsClick = "";
				//-this.researchAreaClick = "";
				//-this.groupSelectedMTClick ="";
				this.resetFilterRegion();
				this.resetFilterResearchArea();
				this.resetFilterTopic();
				this.resetFilterMT();
			},

			resetFilterRegion:function(){
				this.regionClick = "";
			},
			resetFilterResearchArea:function(){
				this.researchAreaClick = "";
			},
			resetFilterTopic:function(){
				this.topicsClick = "";
			},
			resetFilterMT:function(){
				this.groupSelectedMTClick ="";
			},

			searchInputText: function (person) {
				return person.firstName.toLowerCase().indexOf(this.search.toLowerCase()) != -1 ||
					person.lastName.toLowerCase().indexOf(this.search.toLowerCase()) != -1;
			},
			searchLetter: function (person) {

				if (this.letter == "All") {
					return this.persons;
				} else if (this.letter != "All") {
					return (person.firstName.toLowerCase().indexOf(this.letter.toLowerCase()) != -1 ||
						person.lastName.toLowerCase().indexOf(this.letter.toLowerCase()) != -1);
				}


			},

			searchCategory: function (person) {
				console.log(person.region + " \n " +
					person.location + " \n " +
					person.fieldsOfExpertise + " \n " +
					person.researchArea + " \n " +
					person.position + " \n ");
			},

			setChange: function (categories) {
				console.log(categories);
			},
			getCount: function () {
				return this.resultCount;
			},

			setPage: function (pageNumber) {
				this.currentPage = pageNumber;
				/*jQuery('html,body').animate({
					  scrollTop: jQuery("#container-sc").offset().top()
				}, 1500);*/
			},

			setLetter: function (letter) {
				this.$set('letter', letter.letter);
				console.log(" letra seleccionada con click " + this.letter);
			},
			insertImg: function (obj) {
				var out = "";
				this.photosTemp.forEach(function (item) {

					if ((obj == item.carnet) && item.carnet != 0) {
						out = "data:image/png;base64," + item.photo;

					} else {
						out = "http://www.tea-tron.com/antorodriguez/blog/wp-content/uploads/2016/04/image-not-found-4a963b95bf081c3ea02923dceaeb3f8085e1a654fc54840aac61a57a60903fef.png";

					}

				});
				return out;
			},

			erase: function () {
				this.photosTemp = [];
			},

			getImgCarnet: function (carnet) {
				var $json = "";
				var $array = [];
				this.$http.get(uri + '?action=request_action_getPhotoStaff&lc=' + carnet).then(
					function (response) {

						$json = response.data.data[0];

						this.photosTemp.push(JSON.parse(JSON.stringify({
							carnet: $json.carnet,
							//photo:"data:image/png;base64,"+$json.photo,
							photo: "data:image/png;base64," + $json.photo,
							_uidpdiv: $json.carnet + "_idp",
							_uidsdiv: $json.carnet + "_ids",
							_uidpimg: $json.carnet + "_idp",
							_uidsimg: $json.carnet + "_ids"
						})));


					}, function (error) {
						console.log(error);
					});
			},

			getAllStaff: function () {
				var $array = [];
				this.$http.get(uri + '?action=request_action_getAllStaff').then(
					function (response) {

						var $json = JSON.parse(response.body.data.body);
						var $length = Object.keys($json).length;

						this.$set('resultCount', $length);
						this.$set('persons', $json);


					}, function (error) {
						console.log(error);
					});
			},
			getAllAuthors: function () {
				var $array = [];
				this.$http.get(uri + '?action=request_action_getAuthorsBlog').then(
					function (response) {

						console.log(response.data);
						/*var $json = JSON.parse(response.body.data.body);
						var $length = Object.keys($json).length;*/

						//console.log(response);
						//this.$set('resultCount', $length);
						//this.$set('persons', $json);


					}, function (error) {
						console.log(error);
					});
			},

			getPublications: function (person) {
				if (person.publications == undefined) {
				} else {
					console.log(person.publications);
					this.$http.get(uri + '?action=request_action_getPublications&fn=' +
						person.firstName.replace(/\s/g, '') + '&ln=' + person.lastName.replace(/\s/g, '') + '&publications=' +
						person.publications).then(
						function (response) {

							if (response.ok === true && response.status === 200) {
								console.log(" response.ok " + response.ok + "\n");
							}
							var $html = jQuery('<div/>');
							$html.addClass('html-publications');
							var $json = JSON.stringify(response);
							console.log($json);
						}, function (error) {
							console.log(error);
						});
				}
			},

			getListCategories: function () {
				this.$http.get(uri + '?action=request_action_getFilters').then(
					function (response) {
						if (response.ok === true && response.status === 200) {
							console.log(" response.ok " + response.ok + "\n");
						}
						var $json = JSON.parse(response.body.data.return);
						var $length = Object.keys($json).length;
						this.$set('listCategories', $json);
					}, function (error) {
						console.log(error);
					});
			},
			getListTopics: function () {
				this.$http.get(uri + '?action=request_action_getFiltersTopics').then(
					function (response) {
						if (response.ok === true && response.status === 200) {
							console.log(" response.ok " + response.ok + "\n");
						}
						var $json = JSON.parse(response.body.data.return);
						var $length = Object.keys($json).length;
						this.$set('listTopics', $json);
					}, function (error) {
						console.log(error);
					});
			},
			getListPositions: function () {
				this.$http.get(uri + '?action=request_action_getFiltersPositions').then(
					function (response) {
						if (response.ok === true && response.status === 200) {
							console.log(" response.ok " + response.ok + "\n");
						}
						var $json = JSON.parse(response.body.data.return);
						var $length = Object.keys($json).length;
						this.$set('listPositions', $json);
					}, function (error) {
						console.log(error);
					});
			},
			handlerClickCategory: function () {
				this.resetFilters();
				this.$set('categories', this.categories);
				console.log(this.categories);

				//MT = Management Team
				var arrayGroupMT = ["CGIAR Research Programs",
									"Management Team",
									"Office Of The Director General",
									"Research Staff", "Research Support",
									"Research Theme And Program Leaders"];

				for (index in arrayGroupMT) {
					console.log(arrayGroupMT[index] + " " + this.categories.trim());
					if (arrayGroupMT[index].toLowerCase() == this.categories.trim().toLowerCase()) {
						console.log(arrayGroupMT[index] + " FILTRARIA " + this.categories);
						this.groupSelectedMTClick = this.categories.trim();
					}
				}

			},
			/*handlerClickTopics: function () {
				this.$set('topics', this.topics);
			    
			},*/
			handlerClicProfile: function (person) {
				//console.log("Click " + person.firstName);
				this.$set('profileClicked', true);
				this.$set('personClick', person);
				m.getInfo(person);
			}

		},



		filters: {
			capitalizeFirstLetter: function (str) {
				if (str != "" && str != null && str != undefined) {
					var out = unescape(str.charAt(0).toUpperCase() + str.slice(1));
					return out;
				} else {
					return "";
				}


			},
			purifiesTwitterLink: function (strTwitter) {
				var str = "";

				if (strTwitter != "" && strTwitter != null && strTwitter != undefined) {

					if (strTwitter.indexOf('@') != -1) {
						str = strTwitter.substr(1);
						return str;
					} else if (strTwitter.indexOf('@') == -1) {
						if (strTwitter != "" && strTwitter != undefined) {
							return strTwitter;
						} else {
							return "";
						}
					} else if (this.isUrl(strTwitter)) {
						str = strTwitter.split("/")[3];
						return str;
					} else {
						return "";
					}
				}
			},

			purifiesLinkedInProfileLink: function (LinkedInProfileLink) {
				var str = "";

				if (LinkedInProfileLink != "" && LinkedInProfileLink != null && LinkedInProfileLink != undefined) {

					if (this.isUrl(LinkedInProfileLink)) {

						console.log(LinkedInProfileLink);
						return LinkedInProfileLink;

					} else {

						str = "https://co.linkedin.com/in/" + LinkedInProfileLink.split("/")[4];
						console.log(str);
						return str;

					}

				}
			},

			isUrl: function (str) {
				var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
				return regexp.test(str);
			},
			limitChartsBy: function (position, max) {
				if (position != "" && position != null && position != undefined) {
					if (position.length <= max) {
						return position;
					} else if (position.length > max) {
						return position.substring(0, max) + "...";
					} else {
						return position.substring(0, max) + "...";
					}
				} else {
					return null;
				}
			},
			/*startsWith: function (array, person) {
			  if (!this.selectedLetter.length) return array;
				return array.filter(function (element) {
					return element[person].toLowerCase().indexOf(this.selectedLetter.toLowerCase()) === 0;
			  });
			},*/
			paginate: function (person) {
				this.resultCount = person.length;
				if (this.currentPage >= this.totalPages) {
					this.currentPage = Math.max(0, this.totalPages - 1);
				}
				var index = this.currentPage * this.itemsPerPage;
				return person.slice(index, index + this.itemsPerPage);
			}/*,
				  match: function (value, categories) {
					  return ((value.region == undefined) ? '' : value.region.toLowerCase()).indexOf((this.categories == undefined) ? '' : this.categories.toLowerCase()) > -1 ? value : null ||
						  ((value.fieldsOfExpertise == undefined) ? '' : value.fieldsOfExpertise.toLowerCase()).indexOf((this.categories == undefined) ? '' : this.categories.toLowerCase()) > -1 ? value : null ||
							  ((value.position == undefined) ? '' : value.position.toLowerCase()).indexOf((this.categories == undefined) ? '' : this.categories.toLowerCase()) > -1 ? value : null ||
								  ((value.researchArea == undefined) ? '' : value.researchArea.toLowerCase()).indexOf((this.categories == undefined) ? '' : this.categories.toLowerCase()) > -1 ? value : null;
				  }*/
		}
	});


	/*jQuery(document).ready(function(){
		for(var i=0;i<10;i++){ 
			myViewModel.persons[i].twitter="https://twitter.com/";	 
			myViewModel.persons[i].linkedInProfile="https://www.linkedin.com/company/ciat";
    	}
	});*/


	jQuery(document).ready(function () {
		jQuery('.publications').load(function () {
			jQuery('.publications #main-header').remove();
			jQuery('.publications #top-header').remove();
		});
	});

	jQuery("#publications").contents().find("#main-header").html();

</script>