<?php defined( 'ABSPATH' ) or die( 'Not Access!'); ?>
<?php if($region!==''): ?>
<script>
	var reg = '<?php echo $region; ?>';
	var ass = '<?php echo $assets; ?>';

</script>
<?php endif; ?>

<?php if( defined('WP_REGIONS_STAFF_')&&defined('WP_SUB_REGIONS_STAFF_') ): ?>
<script>
	var envRegions = '<?php echo json_encode(unserialize(WP_REGIONS_STAFF_)); ?>';
	var envSubRegions = '<?php echo json_encode(unserialize(WP_SUB_REGIONS_STAFF_)); ?>';

</script>
<?php endif; ?>

<?php if(defined('WP_PAISES_ALL')&&defined('WP_REGIONES_ALL')):  ?>
<script>
		  var envPaises = '<?php echo json_encode(unserialize(WP_PAISES_ALL)); ?>';
		  var envRegiones = '<?php echo json_encode(unserialize(WP_REGIONES_ALL)); ?>';

</script>
<?php endif; ?>
<div id="container-sc">
	<div id="my_viewR">

		<!--<h2 id="title-staff"  v-show="!profileClicked">
		
		   <?php //echo get_the_title(); ?> 
	   
		</h2>-->



		<!--<div id="select-filter" class="select" name="filter-full" v-show="!profileClicked">
			<select v-model="categories" @change="handlerClickCategory()">
		<option value="">Select a Category</option>
		<option  v-for="list in listCategories" v-bind:value="list.name | lowercase | capitalize">{{ list.name }}</option>		
		</select>
		</div>-->

		<div id="search-form-staff" class="regions-form buscadorshortcode" v-show="!profileClicked">
			<input v-model="search" type="text" placeholder="Enter a name" id="s" debounce="11">
			<input type="submit" id="searchsubmit" value="&#xf002;" class="btn fa-input">
			<br>
		</div>

		<div id="filterLetter" class="filter" v-show="!profileClicked">
			<ul id="letters">
				<li v-for="letter in letters" @click="setLetter(letter)">
					<a id="searchsubmit" href="javascript:void(0);">
						<h6><b>{{letter.letter}}</b>&nbsp;</h6>
					</a>
				</li>
			</ul>
		</div>

		<br>

		<div class="equalHMVWrap regions-eq eqWrap" v-show="!profileClicked">

			<div class="equalHMV eq contenedor" v-for="person in persons | filterBy regionFilterStart | filterBy searchInputText | filterBy searchLetter | paginate">

				<a href="#" @click.stop.prevent="handlerClicProfile(person)">   			  
				
				{{erase()}}
				{{getImgCarnet(person.carnet)}}
				
				<div v-for="photo in photosTemp" id="img-box-{{photo.carnet}}"  v-if="photo.carnet==person.carnet" track-by="_uidsdiv">
					<img  :src="assets"  v-img="photo.photo" v-if="photo.photo.length>26">

					<img  :src="'http://ciat2016.staging.wpengine.com/wp-content/plugins/StaffCIAT/images/image-not-found.png'"  
					v-img="photo.photo" v-if="photo.photo.length<=26">
				</div>

				<div id="legend" >
					<h4>
						{{person.firstName | concat person.lastName  | lowercase | capitalize }}
					</h4>

					<h5>
						{{person.position | limitChartsBy 22 | lowercase | capitalize}}
					</h5>
					<h6 >{{person.researchArea | lowercase | capitalize}}</h6>
				</div>	

				<div id="social" >

					<li class="et-social-icon et-social-twitter-staff" v-if="person.twitter!=null">
					<a href="https://twitter.com/{{ person.twitter | purifiesTwitterLink }}" class="icon" target="_blank"> 
						<span>Twitter</span>
					</a>
			</li>

			<li class="et-social-icon et-social-linkedin-staff" v-if="person.linkedInProfile!=null">
				<a href="{{person.linkedInProfile}}" class="icon" target="_blank">
					<span>Linkedin</span>
				</a>
				<!--<a href="https://www.linkedin.com/company/ciat" class="icon" target="_blank">
					<span>Linkedin</span>
				</a>-->
			</li>

				</div>
			
			</a>

		</div>

		<!--<div id="filterNumbers" v-show="!profileClicked">
			<ul id="numbers">
				<li v-for="pageNumber in totalPages" v-if="Math.abs(pageNumber - currentPage) < 3 || pageNumber == totalPages - 1 || pageNumber == 0">
					<a id="searchsubmit" href="#" @click="setPage(pageNumber)" :class="{current: currentPage === pageNumber, last: (pageNumber == totalPages - 1 && Math.abs(pageNumber - currentPage) > 3), first:(pageNumber == 0 && Math.abs(pageNumber - currentPage) > 3)}">
				   {{ pageNumber+1 }}&nbsp;&nbsp;
				</a>
				</li>
			</ul>
		</div>-->
	</div>


<div id="filterNumbers" v-show="!profileClicked">
		<ul id="numbers" class="pagination justify-content-end">
			<li class="page-item" v-for="pageNumber in totalPages" v-if="Math.abs(pageNumber - currentPage) < 3 || pageNumber == totalPages - 1 || pageNumber == 0">
				<a id="searchsubmit" href="javascript:void(0);" @click.stop.prevent="setPage(pageNumber)" :class="{current: currentPage === pageNumber, last: (pageNumber == totalPages - 1 && Math.abs(pageNumber - currentPage) > 3), first:(pageNumber == 0 && Math.abs(pageNumber - currentPage) > 3) }">
						{{ pageNumber+1 }}
				 </a>
			</li>
		</ul>
</div>

	<div id="regions-header" v-show="profileClicked">
		<h2 id="title-profile" v-show="profileClicked">
			<strong> {{ personClick.firstName | lowercase | capitalize }} &nbsp; {{personClick.lastName | lowercase | capitalize}}</strong>
		</h2>
		<br>
		<h3 id="sub-title-profile" v-show="profileClicked">
			<br> {{personClick.position | lowercase | capitalize}}
		</h3>
		<div class="box-img" v-for="photo in photosTemp" v-if="photo.carnet==personClick.carnet" track-by="_uidpdiv">

			<imagen v-show="profileClicked" :url="photo.photo" :id="photo.carnet" v-if="photo.photo.length>26"></imagen>

			<imagen v-show="profileClicked" :url="'http://ciat2016.staging.wpengine.com/wp-content/uploads/image-not-found.png'" :id="photo.carnet"
				v-if="photo.photo.length<=26">
			</imagen>

		</div>

		<div class="et_pb_module et_pb_tabs et_pb_tabs_0 container-tabs-custom" v-show="profileClicked">
			
			<ul class="et_pb_tabs_controls clearfix custom-tabs">
				<li class="et_pb_tab_0 et_pb_tab_active">
					<a id="tabs-staff">Bio</a>
				</li>
				<li class="et_pb_tab_1">
					<a id="tabs-staff">Blog posts</a>
				</li>
				<li class="et_pb_tab_1">
					<a id="tabs-staff">Publications</a>
				</li>
			</ul>

			<div class="et_pb_all_tabs">
				<div class="et_pb_tab clearfix et_pb_active_content et_pb_tab_0 et-pb-active-slide">
					

					<h6>Description of work:</h6>
					<p>{{personClick.descriptionOfWork  }}</p>
					<h6>Language(s):</h6>
					<p>{{personClick.languages | lowercase | capitalizeFirstLetter }}</p>
					<h6>Location:</h6>
					<p>{{personClick.location | concat personClick.region  | capitalize }} </p>

				</div>

				<div class="et_pb_tab clearfix et_pb_tab_1">

					<p v-if="personClick.ciatBlogProfile!=null">
						<iframe src="http://blog.ciat.cgiar.org/author/{{personClick.ciatBlogProfile}}" class="publications" id="publications" height="400px" width="400px" frameborder="0">

						</iframe>
					</p>
				
					<p v-if="personClick.ciatBlogProfile==null">
						Esta persona no tiene publicaciones en Blog de CIAT, para consultar otros autores 
						consulte  <a src="http://blog.ciat.cgiar.org" target="_blank">blog.ciat.cgiar.org</a>
					</p>

				</div>

				<div class="et_pb_tab clearfix et_pb_tab_2">
					<!--<div id="obj-publications">
						<object class="publications" id="publications" type="text/html" data="{{personClick.publications}}" width="400" height="400"> 
					
					</object>-->

					<iframe src="{{personClick.publications}}" class="publications" id="publications" height="400px" width="400px" frameborder="0">

					</iframe>
					</div>
				</div>
			</div>
		</div>

	</div>
	<div>
	</div>
	{{ testClone() }}
</div>

<script>
	  //	var x2js = new X2JS();

	  //var uri="http://ciat2016.wpengine.com/wp-admin/admin-ajax.php";
	  var uri = "http://ciat.cgiar.org/wp-admin/admin-ajax.php"
	  /*Vue.config.debug = true;
	  Vue.http.options.emulateJSON = true;
	  Vue.http.options.emulateHTTP = true;*/

	  Vue.config.debug = true;
	  Vue.config.silent = true;
	  Vue.config.devtools = true;
	  Vue.http.options.emulateJSON = true;
	  Vue.http.options.emulateHTTP = true;

	  /*Vue.http.interceptors.push((request, next) => {
		  request.url += (request.url.indexOf('?') > 0 ? '&' : '?') + `cb=${new Date().getTime()}`
		  next();
	  });*/



	  //var coontainer=jQuery('#container-sc');  jQuery('.et_pb_row_10').html(coontainer); 

	  Vue.filter('concat', function (value, key) {
		  return value + " " + key;
	  });


	  Vue.directive('img', function (url) {
		  var img = new Image();
		  img.src = url;

		  img.onload = function () {
			  this.el.src = url;
			  jQuery(this.el)
				  .css('opacity', 0.1)
				  .animate({ opacity: 1 }, 311)
		  }.bind(this);


		  /*	img.onload = function() {
	  
				  this.el.src = url;
	  
				  jQuery(this.el).css('opacity', 0).animate({ opacity: 1 }, 311)
	  
			  }.bind(this);*/

	  });

	  var $elem = jQuery(".container-left-sidebar");

	  var imagen = Vue.extend({
		  props: ['url', 'id'],
		  template: '<img :src="url"  id="img-{{id}}" alt="img-{{id}}-alt">'
	  });

	  // modelo funcionalidades Regiones 
	  //var myViewModel= new Vue({ 
	  //var myViewModelR = new Vue({
	  var myViewModelRegionsOut = new Vue({
		  el: '#my_viewR',
		  components: {
			  imagen: imagen
		  },
		  data: {
			  persons: [{
				  location: "",
				  position: "",
				  carnet: 0,
				  region: "",
				  telephone: "",
				  firstName: "",
				  lastName: "",
				  researchArea: "",
				  languages: "",
				  publications: "",
				  descriptionOfWork: "",
				  fieldsOfExpertise: "",
				  emailAddress: "",
				  ciatBlogProfile: "",
				  twitter: "",
				  linkedInProfile: "",
				  researchGateProfile: "",
			  }],
			  letters: [{ letter: "All" }, { letter: "A" }, { letter: "B" }, { letter: "C" }, { letter: "D" }, { letter: "F" }, { letter: "G" },
			  { letter: "H" }, { letter: "I" }, { letter: "J" }, { letter: "K" }, { letter: "L" }, { letter: "M" },
			  { letter: "N" }, { letter: "O" }, { letter: "P" }, { letter: "Q" }, { letter: "R" }, { letter: "S" },
			  { letter: "T" }, { letter: "U" }, { letter: "V" }, { letter: "W" }, { letter: "X" }, { letter: "Y" },
			  { letter: "Z" }],
			  search: "",
			  selectedLetter: "",
			  title: "Staff",
			  
			  currentPage: 1,
			  itemsPerPage: 15,
			  resultCount: 0,

			  str: "",
			  selected: "",
			  filters: [],
			  letter: "",
			  categories: "",
			  listCategories: [],
			  profileClicked: false,
			  left: $elem,
			  rigth: jQuery(""),
			  personClick: [],
			  infoemail: "",
			  staffCarnet: [{ carnet: 0, photo: "" }],
			  photosTemp: [{ carnet: "", photo: "", _uidpdiv: "", _uidsdiv: "", _uidpimg: "", _uidsimg: "" }],
			  tempCarnet: [{ carnet: 0 }],
			  strNCarnets: "",
			  photoTest: "",
			  region: "",
			  arrRegions: [],
			  assets: "",
			  default: "http://ciat2016.staging.wpengine.com/wp-content/uploads/image-not-found.png",

			  envTest: "",
			  envProd: "",
			  eRegions: [],
			  eSubRegions: []
			  /*--,ePaises:[],
			  eRegiones:[]--*/
		  },

		  ready: function () {

			  this.getListCategories();
			  this.getAllStaff();
			  this.$set('region', reg);
			  this.$set('assets', ass + 'images/loader.gif');
			  this.testClone();

			  //this.$set('envTest',envTestURL);
			  // this.$set('envProd',envProductionURL);

			  this.$set('eRegions', JSON.parse(envRegions));
			  this.$set('eSubRegions', JSON.parse(envSubRegions));


			  //--this.$set('ePaises',JSON.parse(envPaises));
			  //--this.$set('eRegiones',JSON.parse(envRegiones));


			  //Unit Test
			  /*console.log(this.ePaises);
			  console.log(this.eRegiones);
			  console.log(this.regionById(0,this.eRegions));
			  console.log(this.regionByName("Latin America and the Caribbean"));
			  console.log(this.subRegionByName("Panama"));
			  console.log(this.regionBelongsToSubregion("Latin America and the Caribbean","Panama"));*/
			  //Unit Test
		  },
		  computed: {

			  totalPages: function () {
				  //console.log(Math.ceil(this.resultCount / this.itemsPerPage) + "totalPages");
				  return Math.ceil(this.resultCount / this.itemsPerPage);
			  }

		  },
		  methods: {

			  list: function (arr) {
				  var out = "";
				  for (obj in arr) {
					  //console.log(JSON.stringify(arr[obj]));
					  out += JSON.stringify(arr[obj]);
				  }
				  return out;
			  },
			  regionById: function (regionId, arr) {
				  var regionOut = {};
				  for (r in arr) {
					  if (arr[r].r_id === regionId) {
						  regionOut = arr[r];
					  }
				  }
				  return regionOut;
			  },

			  regionByName: function (name) {
				  var regionOut = {};
				  var tempName = (name === undefined || name === null) ? "&&_" : name.toLowerCase();

				  for (r in this.eRegions) {
					  //if(this.eRegions[r].r_name.toLowerCase()===name ){
					  if (this.eRegions[r].r_name.toLowerCase().indexOf(tempName) != -1) {
						  //if(this.eRegions[r].r_name.toLowerCase().includes( name )){

						  regionOut = this.eRegions[r];
					  }
				  }
				  return regionOut;
			  },

			  subRegionByName: function (name) {//ingresa PALMIRA COLOMBIA
				  var regionOut = {};
				  //console.log(" TEST UNIT 359 pageStaffRegions "+name);
				  var tempName = (name === undefined || name === null) ? "&&_" : name.toLowerCase();
				  //console.log(" TEST UNIT 361 pageStaffRegions "+tempName);
				  //var size=tempName.length;

				  for (r in this.eSubRegions) {

					  //console.log(" SUB 366 "+this.eSubRegions[r].r_sub_region_name.toLowerCase()+" "+tempName+" "+this.eSubRegions[r].r_sub_region_name.toLowerCase().indexOf(tempName));
					  /*-
					  console.log(" SUB 366 "+this.eSubRegions[r].r_sub_region_name+" "+tempName);*/
					  //if(this.eSubRegions[r].r_sub_region_name.toLowerCase().includes(name)){
					  //if(this.eSubRegions[r].r_sub_region_name.toLowerCase().indexOf(name)!=-1){
					  //if(this.eSubRegions[r].r_sub_region_name.toLowerCase()===name){
					  if (tempName.includes(this.eSubRegions[r].r_sub_region_name.toLowerCase())) {
						  //if(this.eSubRegions[r].r_sub_region_name.toLowerCase().indexOf(tempName)!=-1){
						  //	console.log(" SUB 371 "+this.eSubRegions[r].r_sub_region_name+" "+tempName+" "+this.eSubRegions[r].r_sub_region_name.toLowerCase().indexOf(tempName));	
						  regionOut = this.eSubRegions[r];
					  }

				  }

				  return regionOut;

			  },

			  regionBelongsToSubregion: function (regionStr, subRegionStr) {
				  var flag = false;
				  var region = this.regionByName(regionStr);
				  var subRegion = this.subRegionByName(subRegionStr);
				  //console.log(region.r_id+" "+subRegion.r_regions_r_id);

				  /*console.log(" TEST UNIT 382 pageStaffRegions "+this.region);
				  console.log(" TEST UNIT 383 pageStaffRegions "+regionStr+" "+subRegionStr);
				  console.log(" TEST UNIT 384 pageStaffRegions "+region.r_name+" "+subRegion.r_sub_region_name);
				  console.log(" TEST UNIT 385 pageStaffRegions "+region.r_id+" "+subRegion.r_regions_r_id);*/
				  if (region.r_id === subRegion.r_regions_r_id) {

					  flag = true;
				  }
				  return flag;
			  },


			  testClone: function () {

				  var endDiv = jQuery('#numbers');

				  endDiv.onload = function () {

					  console.log(" CARGO! ");

				  }.bind(this);



			  },
			  /*loader:function(img){
				  console.log(img);
			  },*/
			  clone: function () {
				  //var $l=jQuery('div[class*=et_pb_row_]').length; $l=$l-1; jQuery('.et_pb_row_'+$l);
				  jQuery('.et_pb_row_10').html(jQuery('#container-sc'));
			  },
			  searchInputText: function (person) {
				  return person.firstName.toLowerCase().indexOf(this.search.toLowerCase()) != -1 ||
					  person.lastName.toLowerCase().indexOf(this.search.toLowerCase()) != -1;
			  },

			  regionFilterStart: function (person) {//Cambio logica
				  var searchSize = this.region.length;
				  //console.log("size "+searchSize);
				  //var arrRegions=[];

				  //this.arrRegions.push({name:person.region});
				  //this.arrRegions=jQuery.unique(this.arrRegions);
				  //console.log(this.arrRegions);


				  //var regions=[];
				  /*myViewModelRegion.persons.forEach(function(item,index,array){
  
					  regions.push({name:item.region});
				  });*/
				  //regions=jQuery.unique(regions);
				  //console.log(regions);
				  //var tempRegionFind=(person.region===null||person.region===undefined)?"":person.region;

				  //var tempRegionFind=(person.region===null||person.region===undefined)?"":person.region;
				  //console.log(" Test Unit "+tempRegionFind);

				  //console.log(" Test Unit "+this.regionBelongsToSubregion(this.region,person.region));
				  //console.log(" Test Unit "+person.region);
				  //var verificator=this.regionBelongsToSubregion(this.reg,person.region)!=false?true:false;

				  //return (this.regionBelongsToSubregion(this.reg,person.region))===true?person:null;

				  if (this.regionBelongsToSubregion(this.region, person.region)) {
					  //console.log(" Test Unit 448 pageStaffRegions "+true);
					  return person;
				  } else {
					  //console.log(" Test Unit 451 pageStaffRegions "+false);
					  return null;
				  }
				  /*return ( ((person.region===null||person.region===undefined)?'':person.region.toLowerCase() ).indexOf( this.region.toLowerCase() )!= -1)||
						 (
						 ( ((person.region===null||person.region===undefined)?'':person.region.toLowerCase() ).indexOf( this.region.toLowerCase()  )>= 0         )&&
						 ( ((person.region===null||person.region===undefined)?'':person.region.toLowerCase() ).indexOf( this.region.toLowerCase()  )<= searchSize) 
						 );*/


			  },
			  searchLetter: function (person) {
				  /*return person.firstName.toLowerCase().indexOf(this.letter.toLowerCase()) != -1|| 
						 person.lastName.toLowerCase().indexOf(this.letter.toLowerCase()) != -1;*/
				  if (this.letter == "All") {
					  return this.persons;
				  } else if (this.letter != "All") {
					  return (person.firstName.toLowerCase().indexOf(this.letter.toLowerCase()) != -1 ||
						  person.lastName.toLowerCase().indexOf(this.letter.toLowerCase()) != -1);
				  }
			  },
			  searchCategory: function (person) {
				  console.log(person.region + " \n " +
					  person.location + " \n " +
					  person.fieldsOfExpertise + " \n " +
					  person.researchArea + " \n " +
					  person.position + " \n ");
			  },
			  setChange: function (categories) {
				  console.log(categories);
			  },
			  getCount: function () {
				  return this.resultCount;
			  },
			  setPage: function (pageNumber) {
				  this.currentPage = pageNumber;
			  },
			  setLetter: function (letter) {
				  this.$set('letter', letter.letter);
				  console.log(" letra seleccionada con click " + this.letter);
			  },
			  erase: function () {
				  this.photosTemp = [];
			  },
			  getImgCarnet: function (carnet) {
				  var $json = "";
				  var $array = [];
				  this.$http.get(uri + '?action=request_action_getPhotoStaff&lc=' + carnet).then(
					  function (response) {
						  //console.log( response.data.data[0]);
						  $json = response.data.data[0];


						  this.photosTemp.push(JSON.parse(JSON.stringify({
							  carnet: $json.carnet,
							  //photo:"data:image/png;base64,"+$json.photo,
							  photo: "data:image/png;base64," + $json.photo,
							  _uidpdiv: $json.carnet + "_idp",
							  _uidsdiv: $json.carnet + "_ids",
							  _uidpimg: $json.carnet + "_idp",
							  _uidsimg: $json.carnet + "_ids"
						  })));


					  }, function (error) {
						  console.log(error);
					  });
			  },
			  getAllStaff: function () {
				  var $array = [];
				  this.$http.get(uri + '?action=request_action_getAllStaff').then(
					  function (response) {

						  var $json = JSON.parse(response.body.data.body);
						  var $length = Object.keys($json).length;

						  this.$set('resultCount', $length);
						  this.$set('persons', $json);


					  }, function (error) {
						  console.log(error);
					  });
			  },
			  getPublications: function (person) {
				  if (person.publications == undefined) {
				  } else {
					  console.log(person.publications);
					  this.$http.get(uri + '?action=request_action_getPublications&fn=' +
						  person.firstName.replace(/\s/g, '') + '&ln=' + person.lastName.replace(/\s/g, '') + '&publications=' +
						  person.publications).then(
						  function (response) {

							  if (response.ok === true && response.status === 200) {
								  console.log(" response.ok " + response.ok + "\n");
							  }
							  var $html = jQuery('<div/>');
							  $html.addClass('html-publications');
							  var $json = JSON.stringify(response);
							  console.log($json);
						  }, function (error) {
							  console.log(error);
						  });
				  }
			  },
			  getListCategories: function () {
				  this.$http.get(uri + '?action=request_action_getFilters').then(
					  function (response) {

						  if (response.ok === true && response.status === 200) {
							  console.log(" response.ok " + response.ok + "\n");
						  }
						  var $json = JSON.parse(response.body.data.return);
						  var $length = Object.keys($json).length;


						  this.$set('listCategories', $json);

					  }, function (error) {
						  console.log(error);
					  });
			  },

			  handlerClickCategory: function () {
				  this.$set('categories', this.categories);
				  console.log(this.categories);
			  },
			  handlerClicProfile: function (person) {
				  console.log("Click " + person.firstName);
				  this.$set('profileClicked', true);
				  this.$set('personClick', person);
				  modelRegionsIn.getInfo(person);

			  }
		  },
		  filters: {
			  capitalizeFirstLetter:function(str){
					if (str != "" && str != null && str != undefined) {
						  var out=unescape(str.charAt(0).toUpperCase() + str.slice(1));
						  return out;
					  } else {
						  return "";
					  }

					
				  },
			  purifiesTwitterLink:function(strTwitter){
					 var str="";

					 if (strTwitter != "" && strTwitter != null && strTwitter != undefined) {
						
						 if(strTwitter.indexOf('@') != -1){
							str=strTwitter.substr(1);
							return str;
						 }else if(strTwitter.indexOf('@') == -1){
							if(strTwitter!=""&&strTwitter!=undefined){
 								return strTwitter;
							}else{
								return "";
							}
						 }else if(this.isUrl(strTwitter)){
							str=strTwitter.split("/")[3];
							return str;
						 }else{
							 return "";
						 }
					 }
				  },

				  purifiesLinkedInProfileLink:function(LinkedInProfileLink){
					var str="";

					if (LinkedInProfileLink != "" && LinkedInProfileLink!= null && LinkedInProfileLink != undefined) {
						
						if(this.isUrl(LinkedInProfileLink)){
							
							console.log(LinkedInProfileLink);
							return LinkedInProfileLink;

						}else {
							
							str="https://co.linkedin.com/in/"+LinkedInProfileLink.split("/")[4];
							console.log(str);
							return str;

						}

					} 
				  },

				  isUrl:function(str){
					 var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    				 return regexp.test(str);
				  },

			  limitChartsBy: function (position, max) {
				  if (position != "" && position != null && position != undefined) {
					  if (position.length <= max) {
						  return position;
					  } else if (position.length > max) {
						  return position.substring(0, max) + "...";
					  } else {
						  return position.substring(0, max) + "...";
					  }
				  } else {
					  return null;
				  }
			  },
			  startsWith: function (array, person) {
				  if (!this.selectedLetter.length) return array;
				  return array.filter(function (element) {
					  return element[person].toLowerCase().indexOf(this.selectedLetter.toLowerCase()) === 0;
				  });
			  },
			  paginate: function (person) {
				  this.resultCount = person.length;

				  if (this.currentPage >= this.totalPages) {
					  this.currentPage = Math.max(0, this.totalPages - 1);
				  }
				  var index = this.currentPage * this.itemsPerPage;

				  return person.slice(index, index + this.itemsPerPage);
			  }/*,
			searchLetter:function(value,letter){
				console.log(" letra: "+letter+" personal "+value.firstName);
				return ( (value.firstName==undefined)?'':value.firstName.toLowerCase() ).indexOf( (this.letter==undefined)?'':this.letter.toLowerCase() )===0?value:null||
				       ( (value.lastName==undefined)?'':value.lastName.toLowerCase() ).indexOf( (this.letter==undefined)?'':this.letter.toLowerCase() )===0?value:null     
			}*/,
			  match: function (value, categories) {
				  console.log("Filtra!");

				  return ((value.region == undefined) ? '' : value.region.toLowerCase()).indexOf((this.categories == undefined) ? '' : this.categories.toLowerCase()) > -1 ? value : null ||
					  ((value.fieldsOfExpertise == undefined) ? '' : value.fieldsOfExpertise.toLowerCase()).indexOf((this.categories == undefined) ? '' : this.categories.toLowerCase()) > -1 ? value : null ||
						  ((value.position == undefined) ? '' : value.position.toLowerCase()).indexOf((this.categories == undefined) ? '' : this.categories.toLowerCase()) > -1 ? value : null ||
							  ((value.researchArea == undefined) ? '' : value.researchArea.toLowerCase()).indexOf((this.categories == undefined) ? '' : this.categories.toLowerCase()) > -1 ? value : null;
			  }
		  }

	  });

</script>



<script>
	var endDiv = jQuery('#numbers');

	endDiv.onload = function () {
		console.log(" CARGO! ");
	}.bind(this);


	/*function clone(){
		var $l=jQuery('div[class*=et_pb_row_]').length; 
	    $l=$l-1; 
		jQuery('.et_pb_row_'+$l).html( jQuery('#container-sc') );
	}
	
	clone();*/

	jQuery(document).ready(function () {

		jQuery(window).on('load', function (e) {


			var $l = jQuery('div[class*=et_pb_row_]').length;
			$l = $l - 1;
			console.log($);
			//jQuery('.et_pb_row_' + $l).html(tempHtml);
			jQuery('.et_pb_row_' + $l).append(jQuery('#container-sc'));

			
			//jQuery('.et_pb_row_' + 11).find('div :last-child').after(jQuery('#container-sc'));


			var endDiv = jQuery('#container-sc');

			endDiv.onload = function () {

				console.log(" CARGO! ");

			}.bind(this);


		});

	});


</script>
</div>