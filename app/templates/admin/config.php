<?php  defined( 'ABSPATH' ) or die( 'Not Access!'); ?>
<?php //if( (defined('WP_URI_TEST_')&&defined('WP_URI_PRODUCTION_'))  ): ?>
<script>
//var envTestURL='<?php echo WP_URI_TEST_; ?>';
//var envProductionURL='<?php echo WP_URI_PRODUCTION_; ?>';
</script>
<?php //endif; ?>

<?php //if( defined('WP_REGIONS_STAFF_')&&defined('WP_SUB_REGIONS_STAFF_') ): ?>
<script>
  //var envRegions='<?php echo json_encode(unserialize(WP_REGIONS_STAFF_)); ?>';
  //var envSubRegions='<?php echo json_encode(unserialize(WP_SUB_REGIONS_STAFF_)); ?>';
</script>
<?php //endif; ?>
<!DOCTYPE html>
<html>
    <head>
        <title>Configuracion</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
    </head>
    <body>
        <div>
            <h1>
                Configuration
            </h1>

         <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.24/vue.js"></script>-->

         <div id="content"> 
                <h3> URI TEST ENVIRONMENT </h3>

                <input v-model="env" value="<?php echo WP_URI_TEST_; ?>" type="text" placeholder="URI ENVIRONMENT" id="s" />
                <input type="submit" id="searchsubmit" value="Cambiar" class="btn fa-input">
                
                <h3> URI PRODUCTION ENVIRONMENT </h3>
                <input v-model="env" value="<?php echo WP_URI_PRODUCTION_; ?>" type="text" placeholder="URI ENVIRONMENT" id="s" />
                <input type="submit" id="searchsubmit" value="Cambiar" class="btn fa-input">


                <h3> Regions </h3>
                <br>
                

                <div   v-for="region in eRegions" >
                  
                  <h5>{{region.r_id}} {{region.r_name}} </h5>

                </div>
                <br>
                <div  v-for="subRegion in eSubRegions" >
                  
                  <h5>{{subRegion.r_sub_region_name}} </h5>

                </div>

          </div>

          </div>

          <script>
          Vue.config.debug = false;
		  Vue.http.options.emulateJSON = true;
		  Vue.http.options.emulateHTTP = true;

          var myViewModel = new Vue({ 
		    el: '#content', 
            data:{
                configuration:[{all:""}],
                env:"",
                envTest:"",
                envProd:"",
                eRegions:[],
                eSubRegions:[]
            }
            ,
                 
            ready: function() { 
               /* this.$set('envTest',envTestURL);
                this.$set('envProd',envProductionURL);
                this.$set('eRegions',JSON.parse(envRegions));
                this.$set('eSubRegions',JSON.parse(envSubRegions));
                console.log(this.eSubRegions);*/
                //console.log(JSON.parse(JSON.parse(this.envRegions)));
                //this.list(this.eRegions);

                //console.log(this.regionContainsSubRegion(2));

                //Unit Test
                //-console.log(this.regionById(0,this.eRegions));
                //-console.log(this.regionByName("Latin America and the Caribbean"));
                //-console.log(this.subRegionByName("Panama"));
                //-console.log(this.regionBelongsToSubregion("Latin America and the Caribbean","Panama"));
                //Unit Test

		    },

            methods:{
                
                /*list:function(arr){
                    var out="";
                    for(obj in arr){
                        console.log(JSON.stringify(arr[obj]));
                        out+=JSON.stringify(arr[obj]);
                    }
                    return out;
                },
                regionById:function(regionId,arr){
                    var regionOut={};
                     for(r in arr){
                        if(arr[r].r_id===regionId){
					        regionOut=arr[r];
                        }
                    }
                    return regionOut;
                },
                regionByName:function(name){
                    var regionOut={};
                     for(r in this.eRegions){
                        if(this.eRegions[r].r_name.toLowerCase().indexOf(name.toLowerCase())!=-1){
					        regionOut=this.eRegions[r];
                        }
                    }
                    return regionOut;
                },
                subRegionByName:function(name){
                    var regionOut={};
                     for(r in this.eSubRegions){
                        if(this.eSubRegions[r].r_sub_region_name.toLowerCase().indexOf(name.toLowerCase())!=-1){
					        regionOut=this.eSubRegions[r];
                        }
                    }
                    return regionOut;
                },
                regionBelongsToSubregion:function(regionStr,subRegionStr){
                    var flag=false;
                    var region=this.regionByName(regionStr);
                    var subRegion=this.subRegionByName(subRegionStr);
                    console.log(region.r_id+" "+subRegion.r_regions_r_id);
                    if(region.r_id===subRegion.r_regions_r_id){
                                    flag=true;
                    }
                   return flag;
                }*/


            }

          });

          </script>

          

        
    </body>
</html>
