<?php defined( 'ABSPATH' ) or die( 'Not Access!'); ?>
<div id="my_view">
	<h2 id="title-vue" v-show="!profileClicked">
	 <strong>	<?php echo get_the_title(); ?> </strong></h2>
<!--	<p v-show="!profileClicked">Use quotation marks around a phrase or title for more accurate search results (example:"assistant professor"). </p>-->
	<!-- Se Muestra en pre-profile  -->
	<div id="select-filter" name="filter-full" v-show="!profileClicked">
		<select v-model="categories" @change="handlerClickCategory()">
		<option value="">Select a Category</option>
		<option  v-for="list in listCategories" v-bind:value="list.name | lowercase | capitalize">{{ list.name }}</option>		
		</select>
	</div>
	<!--<div id="search-form-staff" v-show="!profileClicked">
		<input v-model="search" type="text" placeholder="Search" id="s" />
		<input type="submit" id="searchsubmit" value="&#xf002;" class="btn fa-input">
		<br>
	</div>-->
	<div id="filterLetter" v-show="!profileClicked">
		<!-- Se Muestra en pre-profile  -->
		<ul id="letters">
			<li v-for="letter in letters" @click="setLetter(letter)">
				<a id="searchsubmit" href="javascript:void(0);">
					<h6><b>{{letter.letter}}</b>&nbsp;</h6>
				</a>
			</li>
		</ul>
	</div>
	<br>
	<div class="equalHMVWrap eqWrap" v-show="!profileClicked">
		
		<!--<div class="equalHMV eq" v-for="person in persons | filterBy search in 'lastName' | paginate">-->
		<!-- <div class="equalHMV eq" v-for="person in persons | filterBy match in person | paginate | filterBy search in 'firstName'"> -->
		<!--<div class="equalHMV eq" v-for="person in persons | match categories in person | paginate | searchLetter letter in person | filterBy search in 'firstName,lastName'">-->
		<div  class="equalHMV eq"  v-for="person in persons | filterBy searchInputText | filterBy searchLetter | paginate" >
		
<!--			<a href="#" v-bind="getImgCarnet(person)" @click.stop.prevent="handlerClicProfile(person)">   -->
			
			<a href="#"  @click.stop.prevent="handlerClicProfile(person)">   			  
				<!--<img src="http://www.tea-tron.com/antorodriguez/blog/wp-content/uploads/2016/04/image-not-found-4a963b95bf081c3ea02923dceaeb3f8085e1a654fc54840aac61a57a60903fef.png">-->
				{{erase()}}
				{{getImgCarnet(person.carnet)}}
				
				<!--<div  v-for="photo in photosTemp" v-if="photo.carnet==person.carnet" track-by="_uidpdiv">-->
					<!--<img  :src="photo.photo||default"  track-by="_uidpimg">-->
				<!--</div>-->

				<div v-for="photo in photosTemp" id="img-box-{{photo.carnet}}"  v-if="photo.carnet==person.carnet" track-by="_uidsdiv">
					
					<imagen  :url="photo.photo" :id="photo.carnet" v-if="photo.photo.length>26"></imagen>
					
					<imagen  :url="'http://ciat2016.staging.wpengine.com/wp-content/uploads/image-not-found.png'" 
							 :id="photo.carnet" v-if="photo.photo.length<=26"></imagen>
				</div>


				<p>
					<strong> {{ person.firstName | lowercase | capitalize }} &nbsp; {{person.lastName | lowercase | capitalize}}</strong>
					<br> {{person.position | lowercase | capitalize}}
					<br> {{person.researchArea | lowercase | capitalize}}
					<div id="social">
						<!--<ul id="social-icons">-->
						<!--<li>-->
						<a v-bind:href="person.twitter">
							<img class="social" src="<?php echo WP_PLUGIN_ASSETS_PUBLIC_IMGES.'twitter-logo-16.png'; ?>" alt="social-twitter">
							</a>
						<!--</li>-->
						<!--<li>-->
						<a href="{{person.linkedInProfile}}">
							<img class="social" src="<?php echo WP_PLUGIN_ASSETS_PUBLIC_IMGES.'linkedin-logo-16.png'; ?>" alt="social-twitter">
							</a>
						<!--</li>-->
						<!--</ul>-->
					</div>
				</p>
			</a>
		</div>
		<div id="filterNumbers" v-show="!profileClicked">
			<ul id="numbers">
				<li v-for="pageNumber in totalPages" v-if="Math.abs(pageNumber - currentPage) < 3 || pageNumber == totalPages - 1 || pageNumber == 0">
					<a id="searchsubmit" href="#" @click="setPage(pageNumber)" :class="{current: currentPage === pageNumber, last: (pageNumber == totalPages - 1 && Math.abs(pageNumber - currentPage) > 3), first:(pageNumber == 0 && Math.abs(pageNumber - currentPage) > 3)}">
				   {{ pageNumber+1 }}&nbsp;&nbsp;
				</a>
				</li>
			</ul>
		</div>
	</div>
	<!-- Apartir de Aqui se Muestra el perfil del el empleado en caso de presionar click en la fotografia  -->
	<div id="profile-header" v-show="profileClicked">
		<h2 id="title-profile" v-show="profileClicked">
			<strong> {{ personClick.firstName | lowercase | capitalize }} &nbsp; {{personClick.lastName | lowercase | capitalize}}</strong>
		</h2>
		<br>
		<h3 id="sub-title-profile" v-show="profileClicked">
			<br> {{personClick.position | lowercase | capitalize}}
		</h3>
		<div class="box-img" v-for="photo in photosTemp" v-if="photo.carnet==personClick.carnet" track-by="_uidpdiv">
		    <!--<img v-show="profileClicked" src="http://www.tea-tron.com/antorodriguez/blog/wp-content/uploads/2016/04/image-not-found-4a963b95bf081c3ea02923dceaeb3f8085e1a654fc54840aac61a57a60903fef.png">-->
			<!--<img v-show="profileClicked" :src="photo.photo" track-by="_uidsimg">-->
			<!--<image-loader v-show="profileClicked" :src="photo.photo" track-by="_uidsimg" > </image-loader>-->

		    <imagen  v-show="profileClicked"
					 :url="photo.photo" 
					 :id="photo.carnet" 
			         v-if="photo.photo.length>26"></imagen>
					
		    <imagen  v-show="profileClicked"
					 :url="'http://ciat2016.staging.wpengine.com/wp-content/uploads/image-not-found.png'" 
					 :id="photo.carnet" 
					 v-if="photo.photo.length<=26"></imagen>

		</div>
		
			<div class="et_pb_module et_pb_tabs et_pb_tabs_0 container-tabs-custom" v-show="profileClicked"><!-- .container-tabs-custom -->
				<ul class="et_pb_tabs_controls clearfix">
					<li class="et_pb_tab_0 et_pb_tab_active">
						Bio
					</li>
					<li class="et_pb_tab_1">
						Blog posts
					</li>
					<li class="et_pb_tab_1">
						Publications
					</li>
				</ul>
				
				<div class="et_pb_all_tabs">  
					<div class="et_pb_tab clearfix et_pb_active_content et_pb_tab_0 et-pb-active-slide"><!-- Tab Bio -->
						<h6><strong>Description Of Work:</strong> {{personClick.descriptionOfWork | lowercase | capitalize}}</h6>
						<h6><strong>Language(s):</strong> {{personClick.languages | lowercase | capitalize}} </h6>
						<h6><strong>Location:</strong> {{personClick.location | lowercase | capitalize}} </h6>
						<!--<h6><strong>fields Of Expertise:</strong> {{personClick.fieldsOfExpertise | lowercase | capitalize}}</h6>-->
						
						
					</div>	
					<div class="et_pb_tab clearfix et_pb_tab_1">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eget augue dolor. 
						Nam ultricies mauris sit amet quam elementum, at sodales orci porta. Curabitur 
						tincidunt tortor velit, a molestie justo maximus at. Maecenas tristique at arcu 
						lacinia porttitor. Pellentesque habitant morbi tristique senectus et netus et 
						malesuada fames ac turpis egestas. Ut ac gravida turpis. Quisque ullamcorper 
						purus quis libero finibus, vitae feugiat turpis vestibulum.
					</div>
					<div class="et_pb_tab clearfix et_pb_tab_2">
					<div id="obj-publications">
					<object class="publications" id="publications" type="text/html" data="{{personClick.publications}}" width="400" height="400"> 
					
					</object>
					</div>
					</div>
				</div>
			</div>
		
	</div>
	<div>
	</div>
</div>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.24/vue.js"></script>
<script src="https://cdn.jsdelivr.net/vue.resource/1.0.3/vue-resource.min.js"></script>-->
<script>
	var x2js = new X2JS();
	//var base64 = require('node-base64-image');
	//var i64 = require('img-to-64');
	
		//var uri="http://ciat2016.wpengine.com/wp-admin/admin-ajax.php";
	    var uri="http://ciat2016.staging.wpengine.com/wp-admin/admin-ajax.php";
	    Vue.config.debug = false;
		Vue.http.options.emulateJSON = true;
		Vue.http.options.emulateHTTP = true;

	//var fileBase64 = require('vue-file-base64');
//Vue.use(VuePagination);


	/*Vue.http.interceptors.push((request, next) => {
		request.url += (request.url.indexOf('?') > 0 ? '&' : '?') + `cb=${new Date().getTime()}`
		next();
	});*/

	var $elem=jQuery(".container-left-sidebar");
	/*Vue.use(VueImageLoader,{
    	loadInfo: 'Loading',
    	loadError:'Ops..something went wrong',
    	timeout:1
	});*/


	var imagen=Vue.extend({
		props:['url','id'],
		template:'<img :src="url"  id="img-{{id}}" alt="img-{{id}}-alt">'
	});

	var myViewModel = new Vue({ 
		el: '#my_view', 
		/*data:$model*/
		components:{
			imagen:imagen
		},
		data:{
			persons:[{
					location: "",
					position: "",
					carnet:0,
					region: "",
					telephone: "",
					firstName: "",
					lastName: "",
					researchArea: "",
					languages: "",
					publications: "",
					descriptionOfWork: "",
					fieldsOfExpertise: "",
					emailAddress: "",
					ciatBlogProfile: "",
					twitter: "",
					linkedInProfile: "",
					researchGateProfile: "",
					}],
			letters:[{letter:"A"},{letter:"B"},{letter:"C"},{letter:"D"},{letter:"F"},{letter:"G"},
					{letter:"H"},{letter:"I"},{letter:"J"},{letter:"K"},{letter:"L"},{letter:"M"},
					{letter:"N"},{letter:"O"},{letter:"P"},{letter:"Q"},{letter:"R"},{letter:"S"},
					{letter:"T"},{letter:"U"},{letter:"V"},{letter:"W"},{letter:"X"},{letter:"Y"},
					{letter:"Z"}],
			search:"",
			selectedLetter:"",
			title:"Staff",
			currentPage: 1,
			itemsPerPage: 10,
			resultCount: 0,
			str:"",
			selected:"",
			filters:[],
			letter:"",
			categories:"",
			listCategories:[],
			profileClicked:false,
			left:$elem,
			rigth:jQuery(""),
			personClick:[],
			infoemail:"",
			staffCarnet:[{carnet:0,photo:"" }],
			//photosTemp:[{ carnet:0,photo:"" }],
			photosTemp:[{carnet:"", photo:"",_uidpdiv:"",_uidsdiv:"",_uidpimg:"",_uidsimg:""}],
			tempCarnet:[ {carnet: 0 }],
			strNCarnets:"",
			//test:[]
			photoTest:"",
			default:"http://ciat2016.staging.wpengine.com/wp-content/uploads/image-not-found.png"
		},

		ready: function() { 
			this.getListCategories();
			this.getAllStaff();
			
			var onSuccess = function(e){
				document.body.appendChild(e.image);
				alert(e.data);
			 };
				
			var onError = function(e){
				alert(e.message);
			};

			//this.convertToBase64('http://ciat2016.staging.wpengine.com/wp-content/uploads/image-not-found.png');
			
			//this.$set('default',$urlImgDef);
		},
		computed: {
			/*getImg: function() {
				 var out="";
				
				  this.photosTemp.forEach(function (item) {
    			   
					console.log(item.carnet);
					if( (obj==item.carnet)&&item.carnet!=0 ){
						out="data:image/png;base64,"+item.photo;
						
					}else{
						out="http://www.tea-tron.com/antorodriguez/blog/wp-content/uploads/2016/04/image-not-found-4a963b95bf081c3ea02923dceaeb3f8085e1a654fc54840aac61a57a60903fef.png";
						
					}
				
				});
				return out;
      		},*/
			totalPages: function() {
				console.log(Math.ceil(this.resultCount / this.itemsPerPage) + "totalPages");
				return Math.ceil(this.resultCount/ this.itemsPerPage);
        	}
  		},
		methods:{
			convertToBase64:function(url){
			
					var data, canvas, ctx;
					var img = new Image();
  					img.crossOrigin = 'Anonymous';

					img.onload = function(){
						
						canvas = document.createElement('canvas');
						canvas.width = img.width;
						canvas.height = img.height;
						
						ctx = canvas.getContext("2d");
						ctx.drawImage(img, 0, 0);
						

						this.$set('default',canvas.toDataURL());
						console.log(this.default);
					}
					
					
			},
			clear:function(){
				jQuery('.yui-g').hide();
				jQuery('.resultscontrol').hide();
				jQuery('.selectcol').remove();
			},
			convetToJSON:function(content){
				return x2js.xml_str2json( JSON.parse(content ));
			},
			searchInputText: function(person) {
				return person.firstName.toLowerCase().indexOf(this.search.toLowerCase()) != -1|| 
					   person.lastName.toLowerCase().indexOf(this.search.toLowerCase()) != -1;
      		},
			searchLetter: function(person) {
				return person.firstName.toLowerCase().indexOf(this.letter.toLowerCase()) != -1|| 
					   person.lastName.toLowerCase().indexOf(this.letter.toLowerCase()) != -1;
      		}, 
			searchCategory:function(person){
				 console.log(person.region +" \n "+
							 person.location+" \n "+
							 person.fieldsOfExpertise+" \n "+
							 person.researchArea+" \n "+
							 person.position+" \n ");
			},  
			setChange:function(categories){
				console.log(categories);
			},
			getCount:function(){
				return this.resultCount;
			},
			setPage: function(pageNumber) {
          		this.currentPage = pageNumber;
        	},
			setLetter:function(letter){
				this.$set('letter',letter.letter);
				console.log(" letra seleccionada con click "+this.letter);
			},
			photos:function(person){
				var item=[];
				var $json="";
				this.$http.get(uri+'?action=request_action_getPhotoStaff&lc='+person.carnet).then(
                        function (response) {
							//console.log( JSON.parse(response.data.data.body));
							//console.log( response.data.data);
							console.log( response.data.data[0]);
							$json=response.data.data[0];
							this.$set('photoTest',"data:image/png;base64,"+$json.photo);
							
							
							//this.photosTemp.push(JSON.parse(JSON.stringify({carnet:$json.carnet,photo:$json.photo})));
							
						}, function (error) {
                            console.log(error);
                });
			},
		    insertImg:function(obj){
				 var out="";
				
				this.photosTemp.forEach(function (item) {
    			   
					console.log(item.carnet);
					if( (obj==item.carnet)&&item.carnet!=0 ){
						out="data:image/png;base64,"+item.photo;
				
					}else{
						out="http://www.tea-tron.com/antorodriguez/blog/wp-content/uploads/2016/04/image-not-found-4a963b95bf081c3ea02923dceaeb3f8085e1a654fc54840aac61a57a60903fef.png";
				
					}
				
				});
				return out;
			},
			erase:function(){
				this.photosTemp=[];
			},
			getImgCarnet:function(carnet){
				var $json="";
				var $array=[];
				this.$http.get(uri+'?action=request_action_getPhotoStaff&lc='+carnet).then(
                        function (response) {
							//console.log( response.data.data[0]);
							$json=response.data.data[0];

							
							this.photosTemp.push(JSON.parse(JSON.stringify({carnet:$json.carnet,
																			//photo:"data:image/png;base64,"+$json.photo,
																			photo:"data:image/png;base64,"+$json.photo,
																			_uidpdiv:$json.carnet+"_idp",
																			_uidsdiv:$json.carnet+"_ids",
																			_uidpimg:$json.carnet+"_idp",
																			_uidsimg:$json.carnet+"_ids"})));

							
						}, function (error) {
                            console.log(error);
                });
			},
			processCarnets:function(){
				console.log(" EJECUTA CONCATENADOR DE NROS DE CARNET!!");
				var concat="";
				for(var i in this.tempCarnet){ 
					concat+=this.tempCarnet[i].carnet+((i==this.tempCarnet.length-1)?"":","); 
				}
				//console.log(concat); 
				this.$set('strNCarnets',concat);
				this.getStaffAllPhotos();
			},
			getStaffAllPhotos:function(){
				console.log(" EJECUTA RECUPEADOR DE FOTOS!! \n "/*+JSON.stringify(this.strNCarnets)*/);
				//var arrayCarnets=this.strNCarnets.split(',');
				var l=(this.tempCarnet.length/20);
				var $objectTemp=[];
				var cont=0;
				var $json=[];
				var contfase=0;
				for(var i=0;i<this.tempCarnet.length;i++){
					
					if( (l-cont)<=0 ){
							console.log(" Consulta get ajax \n");
							//console.log(" $objectTemp nro "+contfase+" \n"+$objectTemp);
							
                				
						/*	this.$http.get(uri+'?action=request_action_getPhotoStaff&lc='+$objectTemp).then(
								function (response) {
									
									setTimeout(function(){
                						//self.basketAddSuccess = false;
										console.log("JSON nro "+contfase+" \n "+JSON.stringify(response));
            						}, 42000);
								}, function (error) {
									console.log(error);
								});*/
							/*this.$http.get(uri+'?action=request_action_getPhotoStaff&lc='+$objectTemp).then(
								function (response) {
									
									
                					
										console.log("JSON nro "+contfase+" \n "+JSON.stringify(response.body.data.body));
            						
								}, function (error) {
									console.log(error);
								});*/
							contfase++;
					}
					if(cont<=l){
						console.log(" "+(cont++)+" \n ");
						
						$objectTemp.push(this.tempCarnet[i].carnet);
					}else{
						cont=-1;
					}
					cont++;
					
					
				}
				this.$set('staffCarnet',$objectTemp);
				//console.log($objectTemp);
				
				/*this.$http.get(uri+'?action=request_action_getPhotoStaff&lc='+this.strNCarnets).then(
                        function (response) {
							console.log(response);
							//var $json=JSON.parse(response.body.data.body);
							//console.log($json[1]);
							//this.getCarnetPhotos();
						}, function (error) {
                            console.log(error);
                });*/
				
			},
			getAllStaff:function(){
			var $array=[];
			this.$http.get(uri+'?action=request_action_getAllStaff').then(
                        function (response) {
							
							var $json=JSON.parse(response.body.data.body);
							var $length=Object.keys($json).length;
							
							this.$set('resultCount', $length);
							this.$set('persons', $json);
							////////-----/////for(var i in $json){
								////////-----/////$array.push($json[i]);
							////////-----/////}
							////////-----/////this.$set('tempCarnet',$array);
							////////-----/////this.processCarnets();
							//this.getStaffAllPhotos();
							//console.log($json);
							/*this.processCarnets();
							this.getCarnetPhotos();*/
							
						}, function (error) {
                            console.log(error);
                        });
						//this.processCarnets();
						//this.getCarnetPhotos();
						//this.getStaffAllPhotos();
			},
			
			/*getCarnetPhotos:function(){
				//var $out=[];
				//console.log(" person test "+person);
				
				this.$http.get(uri+'?action=request_action_getPhotoStaff&lc='+this.tempCarnet).then(
                        
						function (response) {
							
						console.log(response.body.data.body);	
						var $json=JSON.parse(response.body.data.body);
						//this.$set('tempCarnet',$json);	
						//$out.push($json);
						this.$set('staffCarnet',$json);
					    console.log(this.staffCarnet);
						}, function (error) { 
							console.log(error);
						}
				);
				//this.$set('staffCarnet',$out);
				//return $out;
			},*/
			/*processPhoto:function(){
				
				for(var i in this.persons){
					this.getCarnetPhoto(this.persons[i]);
					console.log(this.persons[i].carnet);
				}
				
			},*/
			/*getCarnetPhoto:function(arr){
				//console.log(person);
				
				this.$http.get(uri+'?action=request_action_getPhotoStaff&lc='+person.carnet).then(
                        function (response) {
							
							//console.log(response);
							var $json=JSON.parse(response.body.data.body);
							
							
							console.log($json);
							
						}, function (error) {
                            console.log(error);
                        });
			},*/
			getPublications:function(person){
				//var array=this.persons;
				
				if(person.publications==undefined){
				}else{
					console.log(person.publications);
				this.$http.get(uri+'?action=request_action_getPublications&fn='+
				               person.firstName.replace(/\s/g,'')+'&ln='+person.lastName.replace(/\s/g,'')+'&publications='+
							   person.publications).then(
                        function (response) {
							
							if(response.ok===true&&response.status===200){
								console.log(" response.ok "+ response.ok+"\n");
								//console.log(" response "+JSON.stringify(response.data.body)+"\n");
								//console.log(" response "+JSON.stringify(response.body)+"\n");
								//console.log(" response "+JSON.stringify(response.body.data.body)+"\n");
								//console.log(" response "+JSON.stringify(response.body.body)+"\n");
							}
							var $html = jQuery('<div/>');
							    $html.addClass('html-publications');
							var $json=JSON.stringify(response);
							console.log($json);
								//$html.data(JSON.parse($json));
							//console.log(this.convetToJSON($json));	
							//var $html=jQuery('div').attr('id','htmlhelper');
							
							//var $json=JSON.stringify(response.body.data.body);
							//console.log(JSON.parse($json));
							//$html.html(JSON.parse($json));
							
							//console.log(jQuery('#htmlhelper a[title^=View ]'));
							
							
							//console.log($html);
							//var $json=JSON.parse(response.data.body);
							//var $length=Object.keys($json).length;
							//console.log($json);
							
							//this.$set('listCategories',$json);
							
						}, function (error) {
                            console.log(error);
                });
				}
			},
			getListCategories:function(){
				this.$http.get(uri+'?action=request_action_getFilters').then(
                        function (response) {
							
							if(response.ok===true&&response.status===200){
								console.log(" response.ok "+ response.ok+"\n");
							}
							var $json=JSON.parse(response.body.data.return);
							var $length=Object.keys($json).length;
							
							
							this.$set('listCategories',$json);
							
						}, function (error) {
                            console.log(error);
                        });
			},
			
			handlerClickCategory:function(){
				this.$set('categories',this.categories);
				console.log(this.categories);
			},
			handlerClicProfile:function(person){
				console.log("Click "+person.firstName);
				this.$set('profileClicked',true);
				this.$set('personClick',person);
				//jQuery(".container-left-sidebar").hide();
				//jQuery(".container-right-sidebar").show();
				//this.$set('infoemail',person.emailAddress);
				//this.getPublications(person);
				//this.getCarnetPhoto(person);
				m.getInfo(person);
				//this.getCarnetPhoto(person);
				
			}
  		},
		filters: {
			startsWith:function(array, person){
			if (!this.selectedLetter.length) return array;
			return array.filter(function(element) {
				return element[person].toLowerCase().indexOf(this.selectedLetter.toLowerCase()) === 0;
			});
			},
			paginate: function(person) {
				this.resultCount = person.length;
				
				if (this.currentPage >= this.totalPages) {
					this.currentPage = Math.max(0, this.totalPages - 1);
				}
				var index = this.currentPage * this.itemsPerPage;
				
				return person.slice(index, index + this.itemsPerPage);
			},
			searchLetter:function(value,letter){
				console.log(" letra: "+letter+" personal "+value.firstName);
				return ( (value.firstName==undefined)?'':value.firstName.toLowerCase() ).indexOf( (this.letter==undefined)?'':this.letter.toLowerCase() )===0?value:null||
				       ( (value.lastName==undefined)?'':value.lastName.toLowerCase() ).indexOf( (this.letter==undefined)?'':this.letter.toLowerCase() )===0?value:null     
			},
			match:function(value,categories){
			console.log("Filtra!");
			
			return (   (value.region==undefined)?'':value.region.toLowerCase()   ).indexOf(   (this.categories==undefined)?'':this.categories.toLowerCase()   )>-1 ? value : null||
				   (   (value.fieldsOfExpertise==undefined)?'':value.fieldsOfExpertise.toLowerCase()   ).indexOf(   (this.categories==undefined)?'':this.categories.toLowerCase()   )>-1 ? value : null||
				   (   (value.position==undefined)?'':value.position.toLowerCase()          ).indexOf(   (this.categories==undefined)?'':this.categories.toLowerCase()   )>-1 ?value:null||
				   (   (value.researchArea==undefined)?'':value.researchArea.toLowerCase()  ).indexOf(   (this.categories==undefined)?'':this.categories.toLowerCase()   )>-1 ?value:null;
			}
    	}
	
});
</script>