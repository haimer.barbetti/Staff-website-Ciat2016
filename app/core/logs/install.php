<?php
defined( 'ABSPATH' ) or die( 'Not Access!' );

global $wpdb;
$wpdb->show_errors();
$charset_collate = $wpdb->get_charset_collate();

$table_name_config = $wpdb->prefix .str_replace(' ','_',$this->getPluginName())."_config";//wp_CiatStaff_config

$regions_json_str='[{"r_id": 0,"r_name": "Asia"},{"r_id": 2,"r_name": "Africa"},{"r_id": 1,"r_name": "Latin America and the Caribbean"}]';

$sub_regions_json_str='[
{"r_sub_region_name": "Myanmar","r_regions_r_id":0},
{"r_sub_region_name": "Cambodia","r_regions_r_id":0},
{"r_sub_region_name": "China","r_regions_r_id": 0},
{"r_sub_region_name": "Indonesia","r_regions_r_id": 0},
{"r_sub_region_name": "Lao PDR","r_regions_r_id": 0}, 
{"r_sub_region_name": "Philippines","r_regions_r_id": 0}, 
{"r_sub_region_name": "Thailand","r_regions_r_id": 0}, 
{"r_sub_region_name": "Vietnam", "r_regions_r_id": 0},
{"r_sub_region_name":"Argentina","r_regions_r_id":1},
{"r_sub_region_name":"Bolivia","r_regions_r_id":1},
{"r_sub_region_name":"Brazil","r_regions_r_id":1},
{"r_sub_region_name":"Chile","r_regions_r_id":1},
{"r_sub_region_name":"Colombia","r_regions_r_id":1},
{"r_sub_region_name":"Costa Rica","r_regions_r_id":1},
{"r_sub_region_name":"Ecuador","r_regions_r_id":1},
{"r_sub_region_name":"El Salvador","r_regions_r_id":1},
{"r_sub_region_name":"Guatemala","r_regions_r_id":1},
{"r_sub_region_name":"Haiti","r_regions_r_id":1},
{"r_sub_region_name":"Honduras","r_regions_r_id":1},
{"r_sub_region_name":"Mexico","r_regions_r_id":1},
{"r_sub_region_name":"Nicaragua","r_regions_r_id":1},
{"r_sub_region_name":"Panama","r_regions_r_id":1},
{"r_sub_region_name":"Peru","r_regions_r_id":1},
{"r_sub_region_name":"Uruguay","r_regions_r_id":1},
{"r_sub_region_name":"Venezuela","r_regions_r_id":1},
{"r_sub_region_name":"Angola","r_regions_r_id": 2},
{"r_sub_region_name":"Burkina Faso","r_regions_r_id": 2},
{"r_sub_region_name":"Burundi","r_regions_r_id": 2},
{"r_sub_region_name":"Cameroon","r_regions_r_id": 2},
{"r_sub_region_name":"Central African Republic","r_regions_r_id": 2},
{"r_sub_region_name":"DR Congo","r_regions_r_id": 2},
{"r_sub_region_name":"Ethiopia","r_regions_r_id": 2},
{"r_sub_region_name":"Ghana","r_regions_r_id": 2},
{"r_sub_region_name":"Guinea","r_regions_r_id": 2},
{"r_sub_region_name":"Kenya","r_regions_r_id": 2},
{"r_sub_region_name":"Lesotho","r_regions_r_id": 2},
{"r_sub_region_name":"Madagascar","r_regions_r_id": 2},
{"r_sub_region_name":"Malawi","r_regions_r_id": 2},
{"r_sub_region_name":"Mali","r_regions_r_id": 2},
{"r_sub_region_name":"Mauritius","r_regions_r_id": 2},
{"r_sub_region_name":"Mozambique","r_regions_r_id": 2},
{"r_sub_region_name":"Rwanda","r_regions_r_id": 2},
{"r_sub_region_name":"Senegal","r_regions_r_id": 2},
{"r_sub_region_name":"Sierra Leone","r_regions_r_id": 2},
{"r_sub_region_name":"South Africa","r_regions_r_id": 2},
{"r_sub_region_name":"South Sudan","r_regions_r_id": 2},
{"r_sub_region_name":"Sudan","r_regions_r_id": 2},
{"r_sub_region_name":"Swaziland","r_regions_r_id": 2},
{"r_sub_region_name":"Tanzania","r_regions_r_id": 2},
{"r_sub_region_name":"Togo","r_regions_r_id": 2},
{"r_sub_region_name":"Uganda","r_regions_r_id": 2},
{"r_sub_region_name":"Zimbabwe","r_regions_r_id": 2},
{"r_sub_region_name":"Zambia","r_regions_r_id": 2}]';


$sql = "CREATE TABLE $table_name_config (
    id mediumint(9) NOT NULL AUTO_INCREMENT,
    ws_uri_production_env varchar(255) NOT NULL,
    ws_uri_test_env varchar(255) NOT NULL,
    ws_env_state int(2) ,
    ws_r_regions MEDIUMTEXT,
    ws_r_sub_regions MEDIUMTEXT,
    ws_env_last_upgrade TIMESTAMP,
    UNIQUE KEY id (id)
) $charset_collate ;";

require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
dbDelta( $sql ); 

//SELECT COUNT(*) FROM wp_Staff_CIAT_config
$select= $wpdb->get_row( "SELECT COUNT(*) FROM  $table_name_config ", ARRAY_N );

//ws_env_state{1=prodution,0=staging}
//print_r($select[0]);
$r_regions_json=json_decode($regions_json_str,true);
$r_sub_regions_json=json_decode($sub_regions_json_str,true);
$serialized=maybe_serialize($r_regions_json);
$serializedSub=maybe_serialize($r_sub_regions_json);

//var_dump($serialized);
//file_put_contents(__DIR__.'/my_loggg.txt', ob_get_contents());

if($select[0]==0){
    
    $insert=" INSERT INTO ".$table_name_config."(ws_uri_production_env, ws_uri_test_env, ws_env_state,ws_r_regions,ws_r_sub_regions)".
            " VALUES ('http://dominio-production-uri-ws','http://dominio-staging-uri-ws',0,'".$serialized."','".$serializedSub."') ";

    /*$insert=" INSERT INTO $table_name_config (ws_uri_production_env, ws_uri_test_env, ws_env_state) 
                     VALUES ('http://dominio-production-uri-ws','http://dominio-staging-uri-ws',0) ";*/

    $queryState=$wpdb->query($insert); 
    
    $varsEnv= $wpdb->get_row( "SELECT DISTINCT id,
                                          ws_uri_production_env,
                                          ws_uri_test_env ,
                                          ws_r_regions
                                          ws_r_sub_regions,
                                          ws_env_last_upgrade
                                   FROM   $table_name_config 
                                   WHERE  ws_uri_production_env <> '' AND 
                                          ws_uri_test_env <> '' ", ARRAY_N );

            runkit_constant_redefine('WP_DB_ROW_CONFIG_ID',$varsEnv[0]); 
            runkit_constant_redefine('WP_URI_TEST_',$varsEnv[1]);
            runkit_constant_redefine('WP_URI_PRODUCTION_',$varsEnv[2]);  
            runkit_constant_redefine('WP_REGIONS_STAFF_',$varsEnv[3]);
            runkit_constant_redefine('WP_SUB_REGIONS_STAFF_',$varsEnv[4]);
            runkit_constant_redefine('WP_LAST_UPDATE_STAFF_',$varsEnv[5]);
}else{

    $varsEnv= $wpdb->get_row( "SELECT DISTINCT id,
                                          ws_uri_production_env,
                                          ws_uri_test_env,
                                          ws_r_regions,
                                          ws_r_sub_regions,
                                          ws_env_last_upgrade
                                   FROM   $table_name_config 
                                   WHERE  ws_uri_production_env <> '' AND 
                                          ws_uri_test_env <> '' ", ARRAY_N );
            
            runkit_constant_redefine('WP_DB_ROW_CONFIG_ID',$varsEnv[0]); 
            runkit_constant_redefine('WP_URI_TEST_',$varsEnv[1]);
            runkit_constant_redefine('WP_URI_PRODUCTION_',$varsEnv[2]);  
            runkit_constant_redefine('WP_REGIONS_STAFF_',$varsEnv[3]);  
            runkit_constant_redefine('WP_SUB_REGIONS_STAFF_',$varsEnv[4]);
            runkit_constant_redefine('WP_LAST_UPDATE_STAFF_',$varsEnv[5]);
}

