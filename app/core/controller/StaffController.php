<?php
defined( 'ABSPATH' ) or die( 'No Access!' );
global $wpdb;
class StaffController  {

public function __construct() {


    add_action('wp_ajax_request_action_askForChangePass',array($this,'request_action_askForChangePass'));
    add_action('wp_ajax_nopriv_request_action_askForChangePass', array($this,'request_action_askForChangePass'));

    add_action('wp_ajax_request_action_getAllStaff',array($this,'request_action_getAllStaff'));
    add_action('wp_ajax_nopriv_request_action_getAllStaff', array($this,'request_action_getAllStaff'));


    add_action('wp_ajax_request_action_getFilters',array($this,'request_action_getFilters'));
    add_action('wp_ajax_nopriv_request_action_getFilters', array($this,'request_action_getFilters'));
    
    add_action('wp_ajax_request_action_getPublications',array($this,'request_action_getPublications'));
    add_action('wp_ajax_nopriv_request_action_getPublications', array($this,'request_action_getPublications'));
    
    add_action('wp_ajax_request_action_getPhotoStaff',array($this,'request_action_getPhotoStaff'));
    add_action('wp_ajax_nopriv_request_action_getPhotoStaff', array($this,'request_action_getPhotoStaff'));

    /*add_action('wp_ajax_tester',array($this,'tester'));
    add_action('wp_ajax_nopriv_tester', array($this,'tester'));*/

    //add_action('wp_ajax_request_action_getAuthorsBlog',array($this,'request_action_getAuthorsBlog'));
    //add_action('wp_ajax_nopriv_request_action_getAuthorsBlog', array($this,'request_action_getAuthorsBlog'));
    
    add_action('wp_ajax_request_action_getFiltersTopics',array($this,'request_action_getFiltersTopics'));
    add_action('wp_ajax_nopriv_request_action_getFiltersTopics', array($this,'request_action_getFiltersTopics'));

    add_action('wp_ajax_request_action_getFiltersPositions',array($this,'request_action_getFiltersPositions'));
    add_action('wp_ajax_nopriv_request_action_getFiltersPositions', array($this,'request_action_getFiltersPositions'));
    
    
}

public function create_general_trasient(){
    $args=array(
            'method'=> 'GET',
            'timeout'=> 255/*,
            'headers' => array("Content-type" => "application/json" )*/
            );

    $urlAllEmployees=WP_URI_TEST.'allEmployees';
    $urlCarnet=WP_URI_TEST.'imageList?idList=';

    $numEmployees=0;

    $jsonStaff=array();
    $gTrasient=false;


    $response=wp_remote_request($urlAllEmployees,$args);
    
    $body=wp_remote_retrieve_body($response);
    
     $array=json_decode($body,true);

     $numEmployees=count($array);
     $numSec=$numEmployees/4;

    $strCarnets="";
    $cont=0;
    $i=0;
    
     $arrayConcat=array();
     foreach($array as $equipo){
      
        if(($numEmployees-$cont)<=0){
             //$strCarnets="Seccion de ".$i;
        }

        if($cont<=$numEmployees){

            array_push($arrayConcat,array('nroCarnet'=>$equipo['carnet']));
        
        }else{
            $strCarnets.='Seccion de '.$i;
            $cont=$cont-1;
        }
        $cont++;
        $i++;
            
        
    }

    $a=array('method'=> 'GET','timeout'=> 255);
    $newCarnet=array();
    foreach($arrayConcat as $c){


            $responseP=wp_remote_request($urlCarnet.$c['nroCarnet'],$a);
            $bodyP=wp_remote_retrieve_body($responseP);
            $arrayP=json_decode($bodyP,true);
            array_push($newCarnet,array('carnet'=>$arrayP));
            sleep(10);

            //print_r($c['nroCarnet']);
            
    }

    var_dump($newCarnet);

}

 public function request_action_getPhotoStaff(){
     //$nonce=(!isset($_REQUEST['nonce']))?"":$_REQUEST['nonce']; 
     $strCarnet=(!isset($_REQUEST['lc']))?"":$_REQUEST['lc']; 
     //$cb=(!isset($_REQUEST['cb']))?"":$_REQUEST['cb']; 
     //var_dump($cb);
     $args=array('method'=> 'GET',
                 'timeout'=> 255,
                 'compress'=>true);
     
     $transient=get_transient('carnet-'.$strCarnet);

     if($transient==false){
        $response=wp_remote_request(WP_URI_TEST.'imageList?idList='.$strCarnet,$args);
        $body=wp_remote_retrieve_body($response);
        set_transient('carnet-'.$strCarnet, json_decode($body,true), 72*MINUTE_IN_SECONDS);
        $transient=get_transient('carnet-'.$strCarnet);
     }else{
          $transient=get_transient('carnet-'.$strCarnet);
     }

     if ( is_wp_error( $response ) ) {
        $error_message = $response->get_error_message()." ".$response->get_error_code();
        wp_send_json_success($error_message);
     }else{
        wp_send_json_success($transient);
     }
 }

 public function request_action_getPublications(){
     //$nonce=(!isset($_REQUEST['nonce']))?"":$_REQUEST['nonce']; 
     $idfn= (!isset($_REQUEST['fn']))?"":$_REQUEST['fn'];
     $idln= (!isset($_REQUEST['ln']))?"":$_REQUEST['ln']; 
     $url_publications=(!isset($_REQUEST['publications']))?"":$_REQUEST['publications']; 

     //$id=str_replace(' ','-','publications-'. str_replace(' ','-',$idfn).str_replace(' ','-',$idln));

     $args=array(
            'method'=> 'GET',
            'timeout'=>500,
            //'stream' => true,
            'headers' => array( 
                'accept' => 'application/xml',
                "Content-type" => "application/json" ),
            
            );

     $id='publications-'.$idfn.$idln;
     $transient=get_transient($id); 
        
     if($transient==false){
         
        /* $response=wp_remote_get($url_publications,
                                 array('timeout' => 255),
                                 'headers' => array( "Content-type" => "application/json" ));*/
         
         $response=wp_remote_request($url_publications,$args);

         $body=wp_remote_retrieve_body($response);
         //$return=array('return'=>$body );
         $return=array('return'=>json_decode( json_encode($response)));
         //set_transient($id, $response, DAY_IN_SECONDS);
         set_transient($id, $response, DAY_IN_SECONDS);
         $transient=get_transient($id);
     }else{
        $transient=get_transient($id);
     }

     if ( is_wp_error( $response ) ) {
        $error_message = $response->get_error_message()." ".$response->get_error_code();
        wp_send_json_success($error_message);
    }else{
        wp_send_json_success($response);
    } 
 }

public function request_action_getAllStaff(){
     //$nonce=(!isset($_REQUEST['nonce']))?"":$_REQUEST['nonce']; 
    //$transient=get_transient('staff');
    //$transient=false;
    $transient=get_transient('staff');
    
    if($transient==false){
         
         $response=wp_remote_get(WP_URI_TEST.'allEmployees',array('timeout' => 255,
                                                                  'compress'=>true,
                                                                  'stream'=>false));
         $body=wp_remote_retrieve_body($response);
         $return=array('return'=>$body );

         set_transient('staff', $response, 40*MINUTE_IN_SECONDS);
         $transient=get_transient('staff');

     }else{
         $transient=get_transient('staff');
     }

    if ( is_wp_error( $response ) ) {
        $error_message = $response->get_error_message()." ".$response->get_error_code();
        wp_send_json_success($error_message);
    }else{
        wp_send_json_success($transient);
    } 
 } 

public function tester(){
        $post_type = 'filter';
        $taxonomies = get_object_taxonomies( (object) array('post_type' =>$post_type ) );
        $a=array();

        $transient=get_transient('filters'); 
        $transient=false;

        if(false==$transient){
		foreach($taxonomies as $taxonomy ) : 
		
            $terms = get_terms( $taxonomy );
			
            foreach( $terms as $term ) : 
                 
				$posts = new WP_Query("taxonomy=$taxonomy&term=$term->slug&posts_per_page=-1&order=ASC&" );
				
                //var_dump($posts);
				//echo "<h4><b>". strtoupper("Filter By ".$term->name)."</b></h4>";
			
                //echo '<div id="filters-'.$term->slug.'">';
				
				if( $posts->have_posts() ): 

                    while( $posts->have_posts() ) : $posts->the_post();  

                        if($term->name=='position / category'){

                            array_push($a,array("name"=>get_the_title()));
                            //the_title();
                        }

                        //the_title(); 
                    
                    endwhile; 
				
			    endif; 
			  
			 endforeach; 

        endforeach;
        //var_dump($a);

        $return=array('return'=>json_encode($a));
        set_transient( 'filters', $return, MINUTE_IN_SECONDS*15);
        $transient=get_transient('filters');

        }else{
             $transient=get_transient('filters'); 
        }

        if (is_wp_error( $response ) ) {
            $error_message = $response->get_error_message()." ".$response->get_error_code();
            wp_send_json_success($error_message);
        }else{
            wp_send_json_success($transient);
        } 
}


 public function request_action_getFilters(){
    $post_type = 'filter';
        $taxonomies = get_object_taxonomies( (object) array('post_type' =>$post_type ) );
        $a=array();

        $transient=get_transient('filters'); 
        if($transient==false){    
		foreach($taxonomies as $taxonomy ) : 
		
            $terms = get_terms( $taxonomy );
			
            foreach( $terms as $term ) : 
                 
				$posts = new WP_Query("taxonomy=$taxonomy&term=$term->slug&posts_per_page=-1&order=ASC&" );
				
                //var_dump($posts);
				//echo "<h4><b>". strtoupper("Filter By ".$term->name)."</b></h4>";
			
                //echo '<div id="filters-'.$term->slug.'">';
				
				if( $posts->have_posts() ): 

                    while( $posts->have_posts() ) : $posts->the_post();  

                        if($term->name=='position / category'){

                            array_push($a,array("name"=>get_the_title()));
                            //the_title();
                        }

                        //the_title(); 
                    
                    endwhile; 
				
			    endif; 
			  
			 endforeach; 

        endforeach;
        //var_dump($a);

        $return=array('return'=>json_encode($a));
        set_transient( 'filters', $return, MINUTE_IN_SECONDS*15);
        $transient=get_transient('filters');

        }else{
             $transient=get_transient('filters'); 
        }

        if (is_wp_error( $response ) ) {
            $error_message = $response->get_error_message()." ".$response->get_error_code();
            wp_send_json_success($error_message);
        }else{
            wp_send_json_success($transient);
        } 
 }

public function request_action_getFiltersTopics(){
    $post_type = 'filter';
        $taxonomies = get_object_taxonomies( (object) array('post_type' =>$post_type ) );
        $a=array();

        $transient=get_transient('topics'); 
        //$transient=false;

        //if(false==$transient){
        if($transient==false){    
		foreach($taxonomies as $taxonomy ) : 
		
            $terms = get_terms( $taxonomy );
			
            foreach( $terms as $term ) : 
                 
				$posts = new WP_Query("taxonomy=$taxonomy&term=$term->slug&posts_per_page=-1&order=ASC&" );
				
                //var_dump($posts);
				//echo "<h4><b>". strtoupper("Filter By ".$term->name)."</b></h4>";
			
                //echo '<div id="filters-'.$term->slug.'">';
				
				if( $posts->have_posts() ): 

                    while( $posts->have_posts() ) : $posts->the_post();  

                        if($term->name=='field of expertise / Topics'){

                            array_push($a,array("name"=>get_the_title()));
                            //the_title();
                        }

                        //the_title(); 
                    
                    endwhile; 
				
			    endif; 
			  
			 endforeach; 

        endforeach;
        //var_dump($a);

        $return=array('return'=>json_encode($a));
        set_transient( 'topics', $return, MINUTE_IN_SECONDS*15);
        $transient=get_transient('topics');

        }else{
             $transient=get_transient('topics'); 
        }

        if (is_wp_error( $response ) ) {
            $error_message = $response->get_error_message()." ".$response->get_error_code();
            wp_send_json_success($error_message);
        }else{
            wp_send_json_success($transient);
        } 
 }

public function request_action_getFiltersPositions(){
        $post_type = 'filter';
        $taxonomies = get_object_taxonomies( (object) array('post_type' =>$post_type ) );
        $a=array();

        $transient=get_transient('positions'); 
        //$transient=false;
        if($transient==false){    
		foreach($taxonomies as $taxonomy ) : 
		
            $terms = get_terms( $taxonomy );
			
            foreach( $terms as $term ) : 
                 
				$posts = new WP_Query("taxonomy=$taxonomy&term=$term->slug&posts_per_page=-1&order=ASC&" );
				
                //var_dump($posts);
				//echo "<h4><b>". strtoupper("Filter By ".$term->name)."</b></h4>";
			
                //echo '<div id="filters-'.$term->slug.'">';
				
				if( $posts->have_posts() ): 

                    while( $posts->have_posts() ) : $posts->the_post();  

                        if($term->name=='position / category'){

                            array_push($a,array("name"=>get_the_title()));
                            //the_title();
                        }

                        //the_title(); 
                    
                    endwhile; 
				
			    endif; 
			  
			 endforeach; 

        endforeach;
        //var_dump($a);

        $return=array('return'=>json_encode($a));
        set_transient( 'positions', $return, MINUTE_IN_SECONDS*15);
        $transient=get_transient('positions');

        }else{
             $transient=get_transient('positions'); 
        }

        if (is_wp_error( $response ) ) {
            $error_message = $response->get_error_message()." ".$response->get_error_code();
            wp_send_json_success($error_message);
        }else{
            wp_send_json_success($transient);
        } 
 }
  /*public function purge_persistence(){
      delete_transient('staff');
      delete_transient('filters');
    
  } */ 

  public function request_action_getAuthorsBlog(){
     //$nonce=(!isset($_REQUEST['nonce']))?"":$_REQUEST['nonce']; 
     //$strCarnet=(!isset($_REQUEST['lc']))?"":$_REQUEST['lc']; 
     $args=array('method'=> 'GET',
                 'timeout'=> 255,
                 'compress'=>true);
     
     $transient=get_transient('autors-blog-'.$strCarnet);

     if(!$transient){
        $response=wp_remote_request('http://blogciat.staging.wpengine.com/wp-json/wp/v2/users/',$args);
        $body=wp_remote_retrieve_body($response);
        set_transient('autors-blog-'.$strCarnet, json_decode($body,true), YEAR_IN_SECONDS);
        $transient=get_transient('autors-blog-'.$strCarnet);
     }else{
          $transient=get_transient('autors-blog-'.$strCarnet);
     }

     if ( is_wp_error( $response ) ) {
        $error_message = $response->get_error_message()." ".$response->get_error_code();
        wp_send_json_success($error_message);
     }else{
        wp_send_json_success($transient);
     }
 }

}

