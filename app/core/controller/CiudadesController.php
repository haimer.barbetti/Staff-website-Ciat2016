<?php
defined( 'ABSPATH' ) or die( 'No Access!' );

class CiudadesController  {
  protected $tableName;


public function __construct() {
  
}

public function ciudadesAll(){
  global $wpdb;
  $paises= $wpdb->get_results("SELECT DISTINCT idpais,pais,descripcion,region_idregion  
                               FROM  wp_Staff_CIAT_pais ", ARRAY_N );
  return $paises;
}

public function ciudadesById($paisId){
  global $wpdb;
  $paises= $wpdb->get_results("SELECT DISTINCT idpais,pais,descripcion,region_idregion  
                               FROM  wp_Staff_CIAT_pais 
                               WHERE idpais=$paisId", ARRAY_N );
  return $paises;
}

public function ciudadesByName($paisName){
  global $wpdb;
  $paises= $wpdb->get_results("SELECT DISTINCT idpais,pais,descripcion,region_idregion  
                                FROM wp_Staff_CIAT_pais
                                WHERE pais LIKE  '%$paisName%' ",ARRAY_N);

  return $paises;
}

public function ciudadesByRegionId($regionId){
  global $wpdb;
  $paises= $wpdb->get_results("SELECT DISTINCT idpais,pais,descripcion,region_idregion  
                               FROM  wp_Staff_CIAT_pais 
                               WHERE region_idregion=$regionId", ARRAY_N );
  return $paises;
}


}

