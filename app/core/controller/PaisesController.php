<?php
defined( 'ABSPATH' ) or die( 'No Access!' );
//global $wpdb;

class PaisesController  {
  protected $tableName;


public function __construct() {
  
}

public function paisesAll(){
  global $wpdb;
  $paises= $wpdb->get_results("SELECT DISTINCT idpais,pais,descripcion,region_idregion  
                               FROM  wp_Staff_CIAT_pais ", ARRAY_N );
  return $paises;
}

public function paisesById($paisId){
  global $wpdb;
  $paises= $wpdb->get_results("SELECT DISTINCT idpais,pais,descripcion,region_idregion  
                               FROM  wp_Staff_CIAT_pais 
                               WHERE idpais=$paisId", ARRAY_N );
  return $paises;
}

public function paisesByName($paisName){
  global $wpdb;
  
  /*$paises= $wpdb->query("SELECT DISTINCT idpais ,pais,descripcion,region_idregion  
                                FROM wp_Staff_CIAT_pais
                                WHERE pais LIKE  '%%s%' ");*/


  $paises=$wpdb->get_col(

          $wpdb->prepare('SELECT DISTINCT idpais ,pais,descripcion,region_idregion
                          FROM wp_Staff_CIAT_pais
                          WHERE pais LIKE "%%s%"',
          $paisName
          )

          ); 

  return $paises;
}

public function paisesByRegionId($regionId){
  global $wpdb;
  $paises= $wpdb->get_results("SELECT DISTINCT idpais,pais,descripcion,region_idregion  
                               FROM  wp_Staff_CIAT_pais 
                               WHERE region_idregion=$regionId", ARRAY_N );
  return $paises;
}


}

