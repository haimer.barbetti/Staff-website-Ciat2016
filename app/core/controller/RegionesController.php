<?php
defined( 'ABSPATH' ) or die( 'No Access!' );

class RegionesController  {
  protected $tableName;

public function __construct() {
  
}

public function regionesAll(){
  global $wpdb;
  $regiones= $wpdb->get_results("SELECT DISTINCT idregion,region,descripcion
                               FROM  wp_Staff_CIAT_region ", ARRAY_N );
  return $regiones;
}

public function regionesById($paisId){
  global $wpdb;
  $regiones= $wpdb->get_results("SELECT DISTINCT idregion,region,descripcion
                               FROM  wp_Staff_CIAT_region
                               WHERE idpais=$paisId", ARRAY_N );
  return $regiones;
}

public function regionesByName($regionName){
  global $wpdb;
  $regiones= $wpdb->get_results("SELECT DISTINCT idregion,region,descripcion
                                FROM wp_Staff_CIAT_region
                                WHERE region LIKE  '%$regionName%' ",OBJECT);

  return $regiones;
}

/*public function regionesByRegionId($regionId){
  global $wpdb;
  $regiones= $wpdb->get_results("SELECT DISTINCT idregion,region,descripcion
                               FROM  wp_Staff_CIAT_region
                               WHERE region_idregion=$regionId", ARRAY_N );
  return $regiones;
}*/


}
