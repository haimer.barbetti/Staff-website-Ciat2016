<?php
defined( 'ABSPATH' ) or die( 'No Access!' );

// Register Custom Post Type

function filters_staff_ciat_post_type() {

	$labels = array(
		'name'                  => 'Filtrers',
		'singular_name'         => 'Filter',
		'menu_name'             => 'Profiles Filters',
		'name_admin_bar'        => 'Profiles Filters',
		'archives'              => 'Filter Archives',
		'parent_item_colon'     => 'Parent Filter:',
		'all_items'             => 'All Filters',
		'add_new_item'          => 'Add New Filter',
		'add_new'               => 'Add Filter',
		'new_item'              => 'New Filter',
		'edit_item'             => 'Edit Filter',
		'update_item'           => 'Update Filter',
		'view_item'             => 'View Filter',
		'search_items'          => 'Search Filter',
		'not_found'             => 'Not found',
		'not_found_in_trash'    => 'Not found in Trash',
		'featured_image'        => 'Featured Image',
		'set_featured_image'    => 'Set featured image',
		'remove_featured_image' => 'Remove featured image',
		'use_featured_image'    => 'Use as featured image',
		'insert_into_item'      => 'Insert into filter',
		'uploaded_to_this_item' => 'Uploaded to this filter',
		'items_list'            => 'Filters list',
		'items_list_navigation' => 'Filters list navigation',
		'filter_items_list'     => 'Filter Filters list',
	);
	$args = array(
		'label'                 => 'Filter',
		'labels'                => $labels,
		'supports'              => array('title','page-attributes'),
		'taxonomies'            => array( 'categories_filters' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => false,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'filter', $args );

}

function categories_filters_staff_ciat_taxonomy() {

	$labels = array(
		'name'                       => 'Categories Filters',
		'singular_name'              => 'Category Filter',
		'menu_name'                  => 'Categories Filters',
		'all_items'                  => 'All Categorys for Filters',
		'parent_item'                => 'Parent Category for Filter',
		'parent_item_colon'          => 'Parent Category for Filter:',
		'new_item_name'              => 'New Category for Filter Name',
		'add_new_item'               => 'Add New Category for Filters',
		'edit_item'                  => 'Edit Category for Filter',
		'update_item'                => 'Update Category for Filter',
		'view_item'                  => 'View Category for Filter',
		'separate_items_with_commas' => 'Separate Categorys for Filters with commas',
		'add_or_remove_items'        => 'Add or remove Categorys for Filters',
		'choose_from_most_used'      => 'Choose from the most used',
		'popular_items'              => 'Popular Categorys for Filters',
		'search_items'               => 'Search Categorys for Filters',
		'not_found'                  => 'Not Found',
		'no_terms'                   => 'No Category for Filter',
		'items_list'                 => 'Categorys for Filters list',
		'items_list_navigation'      => 'Categorys for Filters list navigation',
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'categories_filters', array( 'filter' ), $args );

}
add_action( 'init', 'categories_filters_staff_ciat_taxonomy', 0 );
add_action( 'init', 'filters_staff_ciat_post_type', 0 );