<?php
defined( 'ABSPATH' ) or die( 'Not Access!');

 if ( is_page( 'staff-test' ) ) {
    $page_template= WP_PLUGIN_TEMPLATES.'/custom_templates/page_general_staff.php';
}
return $page_template;