<?php
defined( 'ABSPATH' ) or die( 'Not Access!' );

global $wpdb;
$wpdb->show_errors();
$charset_collate = $wpdb->get_charset_collate();

$table_name_config = $wpdb->prefix .str_replace(' ','_',$this->getPluginName())."_config";

//--$regions_json_str_from_file=file_get_contents(WP__plugin_dir.'app/core/data/regions.json');
//-$regions_json_str=$regions_json_str_from_file;
$regions_json_str='[{"r_id": 0,"r_name": "Asia"},{"r_id": 2,"r_name": "Africa"},{"r_id": 1,"r_name": "Latin America and the Caribbean"}]';

$sub_regions_json_str='[
{"r_sub_region_name": "Myanmar","r_regions_r_id":0},{"r_sub_region_name": "Cambodia","r_regions_r_id":0},
{"r_sub_region_name": "China","r_regions_r_id": 0},{"r_sub_region_name": "Indonesia","r_regions_r_id": 0},
{"r_sub_region_name": "Lao PDR","r_regions_r_id": 0}, {"r_sub_region_name": "Philippines","r_regions_r_id": 0}, 
{"r_sub_region_name": "Thailand","r_regions_r_id": 0}, {"r_sub_region_name": "Vietnam", "r_regions_r_id": 0},
{"r_sub_region_name":"Argentina","r_regions_r_id":1},{"r_sub_region_name":"Bolivia","r_regions_r_id":1},
{"r_sub_region_name":"Brazil","r_regions_r_id":1},{"r_sub_region_name":"Chile","r_regions_r_id":1},
{"r_sub_region_name":"Colombia","r_regions_r_id":1},{"r_sub_region_name":"Costa Rica","r_regions_r_id":1},
{"r_sub_region_name":"Ecuador","r_regions_r_id":1},{"r_sub_region_name":"El Salvador","r_regions_r_id":1},
{"r_sub_region_name":"Guatemala","r_regions_r_id":1},{"r_sub_region_name":"Haiti","r_regions_r_id":1},
{"r_sub_region_name":"Honduras","r_regions_r_id":1},{"r_sub_region_name":"Mexico","r_regions_r_id":1},
{"r_sub_region_name":"Nicaragua","r_regions_r_id":1},{"r_sub_region_name":"Panama","r_regions_r_id":1},
{"r_sub_region_name":"Peru","r_regions_r_id":1},{"r_sub_region_name":"Uruguay","r_regions_r_id":1},
{"r_sub_region_name":"Venezuela","r_regions_r_id":1},{"r_sub_region_name":"Angola","r_regions_r_id": 2},
{"r_sub_region_name":"Burkina Faso","r_regions_r_id": 2},{"r_sub_region_name":"Burundi","r_regions_r_id": 2},
{"r_sub_region_name":"Cameroon","r_regions_r_id": 2},{"r_sub_region_name":"Central African Republic","r_regions_r_id": 2},
{"r_sub_region_name":"DR Congo","r_regions_r_id": 2},{"r_sub_region_name":"Ethiopia","r_regions_r_id": 2},
{"r_sub_region_name":"Ghana","r_regions_r_id": 2},{"r_sub_region_name":"Guinea","r_regions_r_id": 2},
{"r_sub_region_name":"Kenya","r_regions_r_id": 2},{"r_sub_region_name":"Lesotho","r_regions_r_id": 2},
{"r_sub_region_name":"Madagascar","r_regions_r_id": 2},{"r_sub_region_name":"Malawi","r_regions_r_id": 2},
{"r_sub_region_name":"Mali","r_regions_r_id": 2},{"r_sub_region_name":"Mauritius","r_regions_r_id": 2},
{"r_sub_region_name":"Mozambique","r_regions_r_id": 2},{"r_sub_region_name":"Rwanda","r_regions_r_id": 2},
{"r_sub_region_name":"Senegal","r_regions_r_id": 2},{"r_sub_region_name":"Sierra Leone","r_regions_r_id": 2},
{"r_sub_region_name":"South Africa","r_regions_r_id": 2},{"r_sub_region_name":"South Sudan","r_regions_r_id": 2},
{"r_sub_region_name":"Sudan","r_regions_r_id": 2},{"r_sub_region_name":"Swaziland","r_regions_r_id": 2},
{"r_sub_region_name":"Tanzania","r_regions_r_id": 2},{"r_sub_region_name":"Togo","r_regions_r_id": 2},
{"r_sub_region_name":"Uganda","r_regions_r_id": 2},{"r_sub_region_name":"Zimbabwe","r_regions_r_id": 2},
{"r_sub_region_name":"Zambia","r_regions_r_id": 2}]';

$posiciones_management_team_json_str='[
{"p_id":1,"p_management_team":"HEAD PARTNERSHIPS AND COMMUNICATIONS"},
{"p_id":2,"p_management_team":"HEAD PROGRAM CORDINATION - PROGRAM COORDINATION"},
{"p_id":3,"p_management_team":"REGIONAL DIRECTOR - ASIA REGION"},
{"p_id":4,"p_management_team":"REGIONAL DIRECTOR FOR AFRICA - AFRICA REGION"},
{"p_id":5,"p_management_team":"DIRECTOR DE GESTION HUMANA - HUMAN MANAGEMENT"},
{"p_id":6,"p_management_team":"AGROBIODIVERSITY RESEARCH AREA DIRECTOR - AGBIO OTHERS"},
{"p_id":7,"p_management_team":"DIRECTOR DE FINANZAS Y ADMINISTRACION"},
{"p_id":8,"p_management_team":"DECISION AND POLICY ANALYSIS RESEARCH AREA DIRECTOR - DAPA OTHERS"},
{"p_id":9,"p_management_team":"PRINCIPAL SCIENTIST"},
{"p_id":10,"p_management_team":"DIRECTOR GENERAL - DIRECTOR GENERAL OFFICE"}
]';

$posiciones_office_of_director_general_json_str='[
{"p_id":1,"p_office_of_director_general":"ANALISTA ADMINISTRATIVO - DIRECTOR GENERAL OFFICE"},
{"p_id":2,"p_office_of_director_general":"JEFE DE LA OFICINA LEGAL - LEGAL OFFICE"},
{"p_id":3,"p_office_of_director_general":"ASISTENTE SENIOR OFICINA DEL DIRECTOR GENERAL"},
{"p_id":4,"p_office_of_director_general":"SECRETARIA DE LA JUNTA DIRECTIVA Y DEL EQUIPO GERENCIAL - BOARD OFFICE"}]';

$posiciones_research_staff_json_str='[
 {"p_id":1,"p_research_staff":"ASOCIADO DE INVESTIGACION - CASSAVA"},
 {"p_id":2,"p_research_staff":"EXPERTO EN INVESTIGACION - GENETIC RESOURCES"},
 {"p_id":3,"p_research_staff":"ASOCIADO DE INVESTIGACION - TROPICAL FORAGES"},
 {"p_id":4,"p_research_staff":"ASOCIADO DE INVESTIGACION - BEANS"},
 {"p_id":5,"p_research_staff":"EXPERTO EN INVESTIGACION - BEANS"},
 {"p_id":6,"p_research_staff":"ASOCIADO DE INVESTIGACION - BIOTECH"},
 {"p_id":7,"p_research_staff":"ASOCIADO DE INVESTIGACION - RICE"},
 {"p_id":8,"p_research_staff":"EMERITO - CASSAVA"},
 {"p_id":9,"p_research_staff":"EMERITO - TROPICAL FORAGES"},
 {"p_id":10,"p_research_staff":"EMERITO - BEANS"},
 {"p_id":11,"p_research_staff":"EMERITO - BIOTECH"},
 {"p_id":12,"p_research_staff":"EMERITO"},
 {"p_id":13,"p_research_staff":"ASOCIADO DE INVESTIGACION - LINKING FARMERS TO MARKETS"},
 {"p_id":14,"p_research_staff":"EMERITO - RICE"},
 {"p_id":15,"p_research_staff":"ASOCIADO DE INVESTIGACION - GENETIC RESOURCES"},
 {"p_id":16,"p_research_staff":"ASOCIADO DE INVESTIGACION"},
 {"p_id":17,"p_research_staff":"INVESTIGADOR VISITANTE"},
 {"p_id":18,"p_research_staff":"ASOCIADO DE INVESTIGACION - DAPA OTHERS"},
 {"p_id":19,"p_research_staff":"INVESTIGADOR POSTDOCTORAL - RICE"},
 {"p_id":20,"p_research_staff":"INVESTIGADOR POSTDOCTORAL - TROPICAL FORAGES"},
 {"p_id":21,"p_research_staff":"INVESTIGADOR POSTDOCTORAL - BIOTECH"},
 {"p_id":22,"p_research_staff":"ASOCIADO DE INVESTIGACION - ECOSYSTEM SERVICES"},
 {"p_id":23,"p_research_staff":"SCIENTIST - RICE"},
 {"p_id":24,"p_research_staff":"SENIOR SCIENTIST"},
 {"p_id":25,"p_research_staff":"VISITING RESEARCHER"},
 {"p_id":26,"p_research_staff":"SCIENTIFIC OFFICER - DAPA OTHERS"},
 {"p_id":27,"p_research_staff":"RESEARCH ASSOCIATE - DAPA OTHERS"},
 {"p_id":28,"p_research_staff":"CIENTIFICO ASOCIADO"},
 {"p_id":29,"p_research_staff":"PRINCIPAL SCIENTIST, SOIL HEALTH AND CLIMATE CHANGE"},
 {"p_id":30,"p_research_staff":"RESEARCH ASSOCIATE"},
 {"p_id":31,"p_research_staff":"SCIENTIST - CLIMATE CHANGE, AGRICULTURE AND FOOD SECURITY"},
 {"p_id":32,"p_research_staff":"INVESTIGADOR VISITANTE - CLIMATE CHANGE"},
 {"p_id":33,"p_research_staff":"SCIENTIST"},
 {"p_id":34,"p_research_staff":"CIENTIFICO ASOCIADO - SOIL HEALTH"},
 {"p_id":35,"p_research_staff":"CIENTIFICO ASOCIADO - AGBIO OTHERS"},
 {"p_id":36,"p_research_staff":"INVESTIGADOR"},
 {"p_id":37,"p_research_staff":"INVESTIGADOR VISITANTE - RICE"},
 {"p_id":38,"p_research_staff":"SENIOR SCIENTIST - DAPA OTHERS"},
 {"p_id":39,"p_research_staff":"PRINCIPAL SCIENTIST"},
 {"p_id":40,"p_research_staff":"INVESTIGADOR EN BIOINFORMATICA - BIOTECH"},
 {"p_id":41,"p_research_staff":"SENIOR RESEARCH ASSOCIATE - GIS & REMOTE SENSING"},
 {"p_id":42,"p_research_staff":"SENIOR RESEARCH ASSOCIATE - GEOSPATIAL ANALYSIS"},
 {"p_id":43,"p_research_staff":"INVESTIGADORA VISITANTE"},
 {"p_id":44,"p_research_staff":"INVESTIGADOR VISITANTE - CASSAVA"},
 {"p_id":45,"p_research_staff":"INVESTIGADOR VISITANTE - BEANS"},
 {"p_id":46,"p_research_staff":"SCIENTIST - CLIMATE CHANGE"},
 {"p_id":47,"p_research_staff":"ASOCIADO DE INVESTIGACION - SOILS OTHERS"},
 {"p_id":48,"p_research_staff":"RESEARCH ASSOCIATE - CLIMATE/ENVIRONMENTAL ECONOMICS & POLICY"},
 {"p_id":49,"p_research_staff":"SENIOR RESEARCH ASSOCIATE - AGRICULTURAL SYSTEMS"},
 {"p_id":50,"p_research_staff":"RESEARCH ASSOCIATE ¿ CLIMATE SMART AGRICULTURE"}]';


$posiciones_research_support_json_str='[
{"p_id":1,"p_research_support":"TECNICO - BIOTECH"},
{"p_id":2,"p_research_support":"TRABAJADOR - CASSAVA"},
{"p_id":3,"p_research_support":"TRABAJADOR - GENETIC RESOURCES"},
{"p_id":4,"p_research_support":"ASISTENTE DE INVESTIGACION - BIOTECH"},
{"p_id":5,"p_research_support":"LABORATORISTA"},
{"p_id":6,"p_research_support":"TECNICO - RICE"},
{"p_id":7,"p_research_support":"TECNICO - CASSAVA"},
{"p_id":8,"p_research_support":"ASISTENTE DE INVESTIGACION - TROPICAL FORAGES"},
{"p_id":9,"p_research_support":"TECNICO - GENETIC RESOURCES"},
{"p_id":10,"p_research_support":"JEFE DE LABORATORIO -BIOTECH"},
{"p_id":11,"p_research_support":"ASISTENTE DE INVESTIGACION - GENETIC RESOURCES"},
{"p_id":12,"p_research_support":"TRABAJADOR - RICE"},
{"p_id":13,"p_research_support":"TECNICO - TROPICAL FORAGES"},
{"p_id":14,"p_research_support":"TRABAJADOR - TROPICAL FORAGES"},
{"p_id":15,"p_research_support":"TECNICO - BEANS"},
{"p_id":16,"p_research_support":"TECNICO - CENTRAL SERVICES"},
{"p_id":17,"p_research_support":"TECNICO - LATIN AMERICA & THE CARIBBEAN REGION"},
{"p_id":18,"p_research_support":"OPERADOR DE MAQUINARIA AGRICOLA - CENTRAL SERVICES"},
{"p_id":19,"p_research_support":"TRABAJADOR - BEANS"},
{"p_id":20,"p_research_support":"TRABAJADOR - BIOTECH"},
{"p_id":21,"p_research_support":"TECNICO - SOILS OTHERS"},
{"p_id":22,"p_research_support":"ASISTENTE DE INVESTIGACION - BEANS"},
{"p_id":23,"p_research_support":"ASISTENTE DE INVESTIGACION - RICE"},
{"p_id":24,"p_research_support":"JEFE DE LABORATORIO - BIOTECH"},
{"p_id":25,"p_research_support":"TECNICO - HOSTED CENTERS"},
{"p_id":26,"p_research_support":"TRABAJADOR - HOSTED CENTERS"},
{"p_id":27,"p_research_support":"TRABAJADOR"},
{"p_id":28,"p_research_support":"TRABAJADOR - CENTRAL SERVICES"},
{"p_id":29,"p_research_support":"PLANT BREEDER - CASSAVA"},
{"p_id":30,"p_research_support":"AGROBIODIVERSITY RESEARCH AREA DIRECTOR - AGBIO OTHERS"},
{"p_id":31,"p_research_support":"GENETIC RESOURCES PROGRAM LEADER - GENETIC RESOURCES"},
{"p_id":32,"p_research_support":"PLANT BREEDER - HOSTED CENTERS"},
{"p_id":33,"p_research_support":"FITOPATOLOGO - CASSAVA"},
{"p_id":34,"p_research_support":"GEOGRAFIC INFORMAT SYSTEM SPECIALIST - DAPA OTHERS"},
{"p_id":35,"p_research_support":"LINKING FARMERS TO MARKETS SPECIALIST - LINKING FARMERS TO MARKETS"},
{"p_id":36,"p_research_support":"REGIONAL COORDINATOR RESEARCH - BEANS"},
{"p_id":37,"p_research_support":"ASISTENTE DE INVESTIGACION"},
{"p_id":38,"p_research_support":"ESTADISTICO PROGRAMADOR - RICE"},
{"p_id":39,"p_research_support":"ASISTENTE DE INVESTIGACION - SOILS OTHERS"},
{"p_id":40,"p_research_support":"INVESTIGADOR VISITANTE"},
{"p_id":41,"p_research_support":"SEED SYSTEMS SPECIALIST - BEANS"},
{"p_id":42,"p_research_support":"ASISTENTE DE INVESTIGACION - DAPA OTHERS"},
{"p_id":43,"p_research_support":"INVESTIGADOR POSTDOCTORAL - RICE"},
{"p_id":44,"p_research_support":"SOCIAL SCIENTIST - AFRICA REGION"},
{"p_id":45,"p_research_support":"INVESTIGADOR POSTDOCTORAL - TROPICAL FORAGES"},
{"p_id":46,"p_research_support":"INVESTIGADOR POSTDOCTORAL - BIOTECH"},
{"p_id":47,"p_research_support":"SOILS SCIENTIST"},
{"p_id":48,"p_research_support":"ASISTENTE INVESTIGACION 1 - SOILS OTHERS"},
{"p_id":49,"p_research_support":"ASISTENTE INVESTIGACION 1 - LATIN AMERICA & THE CARIBBEAN REGION"},
{"p_id":50,"p_research_support":"ASISTENTE INVESTIGACION - TROPICAL FORAGES"},
{"p_id":51,"p_research_support":"TECNICO 1 - TROPICAL FORAGES"},
{"p_id":52,"p_research_support":"FORAGE BASED CROP-LIVESTOCK SYSTEMS SPECIALIST - TROPICAL FORAGES"},
{"p_id":53,"p_research_support":"AGRICULTURAL MODELLER"},
{"p_id":54,"p_research_support":"AGROENTERPRISE MARKETING SPECIALIST - BEANS"},
{"p_id":55,"p_research_support":"AGRICULTURAL ECONOMIST - BEANS"},
{"p_id":56,"p_research_support":"SENIOR EXPERT VALUE CHAINS AND MARKETS"},
{"p_id":57,"p_research_support":"RESEARCH ASSISTANT"},
{"p_id":58,"p_research_support":"PROGRAM OFFICER - CASSAVA"},
{"p_id":59,"p_research_support":"SCIENTIST - RICE"},
{"p_id":60,"p_research_support":"ASISTENTE DE INVESTIGACION - CASSAVA"},
{"p_id":61,"p_research_support":"ASISTENTE DE INVESTIGACION - HOSTED CENTERS"},
{"p_id":62,"p_research_support":"PLANT PATHOLOGIST - RICE"},
{"p_id":63,"p_research_support":"ASISTENTE DE OPERACIONES DE CAMPO - CENTRAL SERVICES"},
{"p_id":64,"p_research_support":"ECOSYSTEM SERVICES SPECIALIST - ECOSYSTEM SERVICES"},
{"p_id":65,"p_research_support":"SENIOR SCIENTIST"},
{"p_id":66,"p_research_support":"GENETICIST - RICE"},
{"p_id":67,"p_research_support":"TECNICO"},
{"p_id":68,"p_research_support":"BIOLOGO MOLECULAR - BIOTECH"},
{"p_id":69,"p_research_support":"LABORATORY ASSISTANT - BEANS"},
{"p_id":70,"p_research_support":"BEAN BREEDER - BEANS"},
{"p_id":71,"p_research_support":"RESEARCH ASSISTANT - BEANS"},
{"p_id":72,"p_research_support":"TECHNICAL ASSISTANT - BEANS"},
{"p_id":73,"p_research_support":"RICE PRODUCTION SPECIALIST - RICE"},
{"p_id":74,"p_research_support":"ASISTENTE DE INVESTIGACION - ECOSYSTEM SERVICES"},
{"p_id":75,"p_research_support":"TRABAJADOR - LATIN AMERICA & THE CARIBBEAN REGION"},
{"p_id":76,"p_research_support":"ENVIRONMENTAL ECONOMIST - DAPA OTHERS"},
{"p_id":77,"p_research_support":"SOIL SCIENTIST - SOIL HEALTH"},
{"p_id":78,"p_research_support":"PRODUCT DEVELOPMENT & DELIVERY MANAGER - HARVESTPLUS"},
{"p_id":79,"p_research_support":"ENTOMOLOGIST - CASSAVA"},
{"p_id":80,"p_research_support":"LABORATORY TECHNICIAN - SOIL INFORMATION"},
{"p_id":81,"p_research_support":"RESEARCH TECHNICIAN - BEANS"},
{"p_id":82,"p_research_support":"CROP DIVERSITY SPECIALIST"},
{"p_id":83,"p_research_support":"ASISTENTE DE INVESTIGACION - CLIMATE CHANGE"},
{"p_id":84,"p_research_support":"TECHNICIAN - BEANS"},
{"p_id":85,"p_research_support":"SOIL SCIENTIST"},
{"p_id":86,"p_research_support":"SCIENTIFIC OFFICER - DAPA OTHERS"},
{"p_id":87,"p_research_support":"LABORATORY ASSISTANT - SOIL HEALTH"},
{"p_id":88,"p_research_support":"LABORATORY TECHNICIAN - BEANS"},
{"p_id":89,"p_research_support":"JUNIOR EXPERT - TROPICAL FORAGES"},
{"p_id":90,"p_research_support":"RESEARCH ASSISTANT - AFRICA REGION"},
{"p_id":91,"p_research_support":"PRINCIPAL SCIENTIST, SOIL HEALTH AND CLIMATE CHANGE"},
{"p_id":92,"p_research_support":"TECNICO - DAPA OTHERS"},
{"p_id":93,"p_research_support":"IMPACT ASSESSMENT OFFICER - DAPA OTHERS"},
{"p_id":94,"p_research_support":"LABORATORY ASSISTANT - AFRICA REGION"},
{"p_id":95,"p_research_support":"GEO-SPATIAL ANALYST"},
{"p_id":96,"p_research_support":"GENDER AND CLIMATE CHANGE SCIENTIST - DAPA OTHERS"},
{"p_id":97,"p_research_support":"PROGRAM OFFICER - DAPA OTHERS"},
{"p_id":98,"p_research_support":"SCIENTIST - CLIMATE CHANGE, AGRICULTURE AND FOOD SECURITY"},
{"p_id":99,"p_research_support":"CROP DEVELOPMENT SPECIALIST - HARVESTPLUS"},
{"p_id":100,"p_research_support":"RESEARCH ASSISTANT - HARVESTPLUS"},
{"p_id":101,"p_research_support":"STUDENT SUPPORT - SOIL HEALTH"},
{"p_id":102,"p_research_support":"SOCIAL SCIENTIST - ECOSYSTEM SERVICES"},
{"p_id":103,"p_research_support":"ASISTENTE INVESTIGACION - CLIMATE CHANGE"},
{"p_id":104,"p_research_support":"NUTRITION SPECIALIST RWANDA BEAN - AFRICA REGION"},
{"p_id":105,"p_research_support":"CROP PHYSIOLOGIST - BIOTECH"},
{"p_id":106,"p_research_support":"PLANT BREEDER - BEANS"},
{"p_id":107,"p_research_support":"INVESTIGADOR VISITANTE - CLIMATE CHANGE"},
{"p_id":108,"p_research_support":"SEED SYSTEMS SPECIALIST - HARVESTPLUS"},
{"p_id":109,"p_research_support":"ASISTENTE DE INVESTIGACION - AGBIO OTHERS"},
{"p_id":110,"p_research_support":"PROGRAMME TECHNICAL OFFICER - BEANS"},
{"p_id":111,"p_research_support":"LIVESTOCK SYSTEMS SSA - TROPICAL FORAGES"},
{"p_id":112,"p_research_support":"SCIENTIST"},
{"p_id":113,"p_research_support":"AGRIBUSINESS DEVELOPMENT OFFICER - BEANS"},
{"p_id":114,"p_research_support":"REMOTE SENSING/GIS ANALYST - LANDSCAPES"},
{"p_id":115,"p_research_support":"STORES ASSISTANT - HARVESTPLUS"},
{"p_id":116,"p_research_support":"SOCIAL SCIENTIST"},
{"p_id":117,"p_research_support":"SEEDS STORES TECHNICIAN - BEANS"},
{"p_id":118,"p_research_support":"PLANT CELL AND TISSUE CULTURE SPECIALIST - BIOTECH"},
{"p_id":119,"p_research_support":"ENVIRONMENTAL SCIENCE - ECOSYSTEM SERVICES"},
{"p_id":120,"p_research_support":"TRABAJADOR - AGBIO OTHERS"},
{"p_id":121,"p_research_support":"INVESTIGADOR"},
{"p_id":122,"p_research_support":"RESEARCH ASSISTANT - CASSAVA"},
{"p_id":123,"p_research_support":"INVESTIGADOR VISITANTE - RICE"},
{"p_id":124,"p_research_support":"SENIOR SOIL ECOLOGIST"},
{"p_id":125,"p_research_support":"RESEARCH ASSISTANT-PATHOLOGY - AFRICA REGION"},
{"p_id":126,"p_research_support":"MAIZE SEED SYSTEM SPECIALIST - HARVESTPLUS"},
{"p_id":127,"p_research_support":"SEED BUSINESS DEVELOPMENT SPECIALIST - BEANS"},
{"p_id":128,"p_research_support":"RURAL AND RESOURCE ECONOMIST - CASSAVA"},
{"p_id":129,"p_research_support":"TECNICO - AGBIO OTHERS"},
{"p_id":130,"p_research_support":"SENIOR SCIENTIST - DAPA OTHERS"},
{"p_id":131,"p_research_support":"ASISTENTE DE INVESTIGACION - CLIMATE CHANGE, AGRICULTURE AND FOOD SECURITY"},
{"p_id":132,"p_research_support":"CROP ASSISTANT FOR THE NORTH REGION - HARVESTPLUS"},
{"p_id":133,"p_research_support":"ESTUDIANTE PASANTIAS - BIOTECH"},
{"p_id":134,"p_research_support":"LANDSCAPE SCIENTIST"},
{"p_id":135,"p_research_support":"ESTUDIANTE EN PRACTICA"},
{"p_id":136,"p_research_support":"CLIMATE SMART AGRICULTURE"},
{"p_id":137,"p_research_support":"AGRICULTURAL SYSTEMS AND LANDSCAPES SPECIALIST-ASIA"},
{"p_id":138,"p_research_support":"GENDER POSTDOCTORAL FELLOW"},
{"p_id":139,"p_research_support":"AGRICULTURAL FINANCE SPECIALIST"},
{"p_id":140,"p_research_support":"ESTUDIANTE EN PRACTICA - DAPA OTHERS"},
{"p_id":141,"p_research_support":"ESTADISTICO"},
{"p_id":142,"p_research_support":"HUMAN NUTRITIONIST SCIENTIST"},
{"p_id":143,"p_research_support":"TECNICO-FRIJOL - BEANS"},
{"p_id":144,"p_research_support":"IMPACT ASSESSMENT - DAPA OTHERS"},
{"p_id":145,"p_research_support":"SENIOR POLICY EXPERT"},
{"p_id":146,"p_research_support":"PRINCIPAL SCIENTIST"},
{"p_id":147,"p_research_support":"AGRICULTURE DEVELOPMENT ECONOMIST"},
{"p_id":148,"p_research_support":"ESTUDIANTE TESISTA"},
{"p_id":149,"p_research_support":"ASISTENTE DE INVESIGACION"},
{"p_id":150,"p_research_support":"INVESTIGADOR EN BIOINFORMATICA - BIOTECH"},
{"p_id":151,"p_research_support":"INTEGRATED EXPERT FOR VIETNAM REGION"},
{"p_id":152,"p_research_support":"SPECIALIST SCALING FEEDS AND FORAGES TECHNOLOGIES FOR AFRICA REGION"},
{"p_id":153,"p_research_support":"SENIOR RESEARCH ASSOCIATE - GIS & REMOTE SENSING"},
{"p_id":154,"p_research_support":"SENIOR CLIMATE AND ECOSYSTEMS SCIENTIS"},
{"p_id":155,"p_research_support":"BIOMETRICIAN"},
{"p_id":156,"p_research_support":"ASISTENTE D INVESTIGACION"},
{"p_id":157,"p_research_support":"SENIOR RESEARCH ASSOCIATE - GEOSPATIAL ANALYSIS"},
{"p_id":158,"p_research_support":"INVESTIGADORA VISITANTE"},
{"p_id":159,"p_research_support":"LABORATORISTA - CENTRAL SERVICES"},
{"p_id":160,"p_research_support":"CLIMATE CHANGE COMMUNICATION SPECIALIST"},
{"p_id":161,"p_research_support":"CLIMATE STRATEGY SPECIALIST"},
{"p_id":162,"p_research_support":"INVESTIGADOR VISITANTE - CASSAVA"},
{"p_id":163,"p_research_support":"AGRONOMIST - DAPA OTHERS"},
{"p_id":164,"p_research_support":"AGRONOMIST - CLIMATE CHANGE"},
{"p_id":165,"p_research_support":"FORAGE BASED CROP-LIVESTOCK SYSTEMS SPECIALIST"},
{"p_id":166,"p_research_support":"INVESTIGADOR VISITANTE - BEANS"},
{"p_id":167,"p_research_support":"SCIENTIST - CLIMATE CHANGE"},
{"p_id":168,"p_research_support":"SOCIO-ECONOMIST - TROPICAL FORAGES"},
{"p_id":169,"p_research_support":"SENIOR RESEARCH ASSOCIATE - AGRICULTURAL SYSTEMS"},
{"p_id":170,"p_research_support":"RESEARCH ASSISTANT - GIS & REMOTE SENSING"},
{"p_id":171,"p_research_support":"CLIMATE SMART AGRICULTURE - PROFILES COORDINATOR"},
{"p_id":172,"p_research_support":"PLANT BREEDER - TROPICAL FORAGES"},
{"p_id":173,"p_research_support":"RESEARCH ASSISTANT - GEOSPATIAL ANALYSIS AND DATA MANAGEMENT"},
{"p_id":174,"p_research_support":"CASSAVA PRODUCTION SYSTEMS SPECIALIST"}]';

$posiciones_research_theme_and_program_leaders_json_str='[
{"p_id":0,"p_research_theme_and_program_leaders":"GENETIC RESOURCES PROGRAM LEADER - GENETIC RESOURCES"},
{"p_id":1,"p_research_theme_and_program_leaders":"TROPICAL FORAGES PROGRAM LEADER - TROPICAL FORAGES"},
{"p_id":2,"p_research_theme_and_program_leaders":"LEADER - DATA, INFORMATION, KNOWLEDGE GROUP"},
{"p_id":3,"p_research_theme_and_program_leaders":"CASSAVA PROGRAM LEADER"},
{"p_id":4,"p_research_theme_and_program_leaders":"LIDER DE PROGRAMA REGIONAL DE CCAFS PARA AMERICA LATINA - CLIMATE CHANGE, AGRICULTURE AND FOOD SECURITY"},
{"p_id":5,"p_research_theme_and_program_leaders":"BEAN PROGRAM LEADER - BEANS"},
{"p_id":6,"p_research_theme_and_program_leaders":"RICE PROGRAM LEADER - RICE"}
]';

$sql = "CREATE TABLE $table_name_config (
                        id mediumint(9) NOT NULL AUTO_INCREMENT,
                        ws_uri_production_env varchar(255) NOT NULL,
                        ws_uri_test_env varchar(255) NOT NULL,
                        ws_env_state int(2) ,
                        ws_r_regions MEDIUMTEXT,
                        ws_r_sub_regions MEDIUMTEXT,
                        ws_env_last_upgrade TIMESTAMP,
                        ws_p_cgiar_research_programs LONGTEXT,
                        ws_p_management_team LONGTEXT,
                        ws_p_office_of_the_director_general LONGTEXT,
                        ws_p_research_staff LONGTEXT,
                        ws_p_research_support LONGTEXT,
                        ws_p_research_theme_and_program_leaders LONGTEXT,
                        UNIQUE KEY id (id)
) $charset_collate ;";

require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
dbDelta( $sql ); 

//SELECT COUNT(*) FROM wp_Staff_CIAT_config
$select= $wpdb->get_row( "SELECT COUNT(*) FROM  $table_name_config ", ARRAY_N );

//ws_env_state{1=prodution,0=staging}
//print_r($select[0]);

$r_regions_json=json_decode($regions_json_str,true);
$r_sub_regions_json=json_decode($sub_regions_json_str,true);

$serialized=maybe_serialize($r_regions_json);
$serializedSub=maybe_serialize($r_sub_regions_json);

//------
$p_management_team_json=json_decode($posiciones_management_team_json_str,true);
$p_office_of_director_general_json=json_decode($posiciones_office_of_director_general_json_str,true);
$p_research_staff_json=json_decode($posiciones_research_staff_json_str,true);
$p_research_support_json=json_decode($posiciones_research_support_json_str,true);
$p_research_theme_and_program_leaders_json=json_decode($posiciones_research_theme_and_program_leaders_json_str,true);


$serializedManagementTeam=maybe_serialize($p_management_team_json);
$serializedOODGeneral=maybe_serialize($p_office_of_director_general_json);
$serializedRStaff=maybe_serialize($p_research_staff_json);
$serializedRSupport=maybe_serialize($p_research_support_json);
$serializedRTAProgramLeaders=maybe_serialize($p_research_theme_and_program_leaders_json);

if($select[0]==0){
    
    /*$insert=" INSERT INTO ".$table_name_config."(ws_uri_production_env, ws_uri_test_env, ws_env_state,ws_r_regions,ws_r_sub_regions)".
            " VALUES ('http://dominio-production-uri-ws','http://dominio-staging-uri-ws',0,'".$serialized."','".$serializedSub."') ";*/

        /*
        ws_p_cgiar_research_programs LONGTEXT,
                        ws_p_management_team LONGTEXT,
                        ws_p_office_of_the_director_general LONGTEXT,
                        ws_p_research_staff LONGTEXT,
                        ws_p_research_support LONGTEXT,
                        ws_p_research_theme_and_program_leaders LONGTEXT,
                        */


    $insert=" INSERT INTO "
            .$table_name_config
            ."(ws_uri_production_env,"
            ." ws_uri_test_env,"
            ." ws_env_state,"
            ." ws_r_regions,"
            ." ws_r_sub_regions,"
            
            ." ws_p_management_team,"
            ." ws_p_office_of_the_director_general,"
            ." ws_p_research_staff,"
            ." ws_p_research_support,"
            ." ws_p_research_theme_and_program_leaders)"
            ." VALUES ('http://dominio-production-uri-ws',"
                    ." 'http://dominio-staging-uri-ws',0,'"
                       .$serialized."','"
                       .$serializedSub."','"
                       .$serializedManagementTeam."','"
                       .$serializedOODGeneral."','"
                       .$serializedRStaff."','"
                       .$serializedRSupport."','"
                       .$serializedRTAProgramLeaders."') ";

    $queryState=$wpdb->query($insert); 
    
    $varsEnv= $wpdb->get_row( "SELECT DISTINCT id,
                                          ws_uri_production_env,
                                          ws_uri_test_env ,
                                          ws_r_regions
                                          ws_r_sub_regions,
                                          ws_env_last_upgrade,

                                          ws_p_cgiar_research_programs,
                                          ws_p_management_team,
                                          ws_p_office_of_the_director_general,
                                          ws_p_research_staff,
                                          ws_p_research_support,
                                          ws_p_research_theme_and_program_leaders

                                   FROM   $table_name_config 
                                   WHERE  ws_uri_production_env <> '' AND 
                                          ws_uri_test_env <> '' ", ARRAY_N );

            runkit_constant_redefine('WP_DB_ROW_CONFIG_ID',$varsEnv[0]); 
            runkit_constant_redefine('WP_URI_TEST_',$varsEnv[1]);
            runkit_constant_redefine('WP_URI_PRODUCTION_',$varsEnv[2]);  
            runkit_constant_redefine('WP_REGIONS_STAFF_',$varsEnv[3]);
            runkit_constant_redefine('WP_SUB_REGIONS_STAFF_',$varsEnv[4]);
            runkit_constant_redefine('WP_LAST_UPDATE_STAFF_',$varsEnv[5]);

            runkit_constant_redefine('WP_CGIAR_RESEARCH_PROGRAMS_',$varsEnv[6]);
            runkit_constant_redefine('WP_MANAGEMENT_TEAM_',$varsEnv[7]);
            runkit_constant_redefine('WP_OFFICE_OF_THE_DIRECTOR_GENERAL_',$varsEnv[8]);
            runkit_constant_redefine('WP_RESEARCH_STAFF_',$varsEnv[9]);
            runkit_constant_redefine('WP_RESEARCH_SUPPORT_',$varsEnv[10]);
            runkit_constant_redefine('WP_RESEARCH_THEME_AND_PROGRAM_LEADERS_',$varsEnv[11]);
}else{

    $varsEnv= $wpdb->get_row( "SELECT DISTINCT id,
                                          ws_uri_production_env,
                                          ws_uri_test_env,
                                          ws_r_regions,
                                          ws_r_sub_regions,
                                          ws_env_last_upgrade,

                                          ws_p_cgiar_research_programs,
                                          ws_p_management_team,
                                          ws_p_office_of_the_director_general,
                                          ws_p_research_staff,
                                          ws_p_research_support,
                                          ws_p_research_theme_and_program_leaders

                                   FROM   $table_name_config 
                                   WHERE  ws_uri_production_env <> '' AND 
                                          ws_uri_test_env <> '' ", ARRAY_N );
            
            runkit_constant_redefine('WP_DB_ROW_CONFIG_ID',$varsEnv[0]); 
            runkit_constant_redefine('WP_URI_TEST_',$varsEnv[1]);
            runkit_constant_redefine('WP_URI_PRODUCTION_',$varsEnv[2]);  
            runkit_constant_redefine('WP_REGIONS_STAFF_',$varsEnv[3]);  
            runkit_constant_redefine('WP_SUB_REGIONS_STAFF_',$varsEnv[4]);
            runkit_constant_redefine('WP_LAST_UPDATE_STAFF_',$varsEnv[5]);

            runkit_constant_redefine('WP_CGIAR_RESEARCH_PROGRAMS_',$varsEnv[6]);
            runkit_constant_redefine('WP_MANAGEMENT_TEAM_',$varsEnv[7]);
            runkit_constant_redefine('WP_OFFICE_OF_THE_DIRECTOR_GENERAL_',$varsEnv[8]);
            runkit_constant_redefine('WP_RESEARCH_STAFF_',$varsEnv[9]);
            runkit_constant_redefine('WP_RESEARCH_SUPPORT_',$varsEnv[10]);
            runkit_constant_redefine('WP_RESEARCH_THEME_AND_PROGRAM_LEADERS_',$varsEnv[11]);
}

