<?php
defined('ABSPATH') or die( 'No Access!');
$p_name=str_replace(' ','-',$this->getPluginName());

//wp_register_style($p_name.'-style',WP_PLUGIN_ASSETS.'css/styles-p.css',array(),'1.0.1','all');



wp_register_style($p_name.'-style',WP_PLUGIN_ASSETS_PUBLIC_CSS.'styles-p.css',array(),'1.0.6','all');

wp_register_style($p_name.'-bootstrap',
                     'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
                     '3.3.7','all');

wp_enqueue_style($p_name.'-style');
wp_enqueue_style($p_name.'-bootstrap');
