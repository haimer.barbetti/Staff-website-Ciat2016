<?php
defined( 'ABSPATH' ) or die( 'Not Access !' );


require_once (WP__plugin_dir.'app/core/interfaces/Singleton.php');


class Cache  implements Singleton {

	protected $transient;
	private static $instance;
	
	public function __construct(){  
        
	}
	
	
	public static function getInstance(){
		
		if(self::$instance == null) {
			
			self::$instance = new ElementHtml();
			
		}
		
		return self::$instance;
		
	}
	
	
	public function __clone(){
	}
	
	
	public function __wakeup(){
	}
	
	
	
	/*     
     * Apartir de aqui las funciones      
     */
	
	public static function getCache($str){
        $this->transient=get_transient($str);    
	}

    public static function setCache($str){
        return $this->transient;
    }

    public static function clearCacheAll(){

    }



}
