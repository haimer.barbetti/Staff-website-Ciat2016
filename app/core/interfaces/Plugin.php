<?php
defined( 'ABSPATH' ) or die( 'No Access!' );
/**
 *
 * @author hbarbetti
 */
interface Plugin {

    public function install();

    public function uninstall();

    //public function exist_WP_function($funct);

    public function styles_up();

    public function scripts_up();

    //public function request_actions();
    public function taxonomies_up();

    public function allow_origin();

    public function allow_custom_host( $allow, $host, $url );

    public function sidebar_up();

    public function add_admin_menu();

    //public function conjobs_up();

    public function init();
}
