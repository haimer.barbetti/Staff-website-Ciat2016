<?php

/**
 *
 * @author hbarbetti
 */
interface Singleton {

    public static function getInstance();
    function __clone();
    function __wakeup();
}
