#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-03-03 21:11+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/"

#. Name of the plugin
msgid "Staff CIAT"
msgstr ""

#. Description of the plugin
msgid ""
"This project implements a plugin for the web site of the Organization CIAT, "
"for the presentation of the staff in several of its website and other "
"functionalities."
msgstr ""

#. URI of the plugin
msgid "https://gitlab.com/haimer.barbetti/Staff-website-Ciat2016.git"
msgstr ""

#. Author of the plugin
msgid "CIAT - Haimer Barbetti | Alejandro Marulanda"
msgstr ""

#. Author URI of the plugin
msgid "https://gitlab.com/haimer.barbetti"
msgstr ""
